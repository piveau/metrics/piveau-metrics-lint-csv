# Indicators 

The indicators are based on the indicators from the [csvlint.rb Ruby gem](https://github.com/Data-Liberation-Front/csvlint.rb) with
additions from the [Data.europa.eu data quality guidelines ](https://op.europa.eu/en/publication-detail/-/publication/023ce8e4-50c8-11ec-91ac-01aa75ed71a1/language-en) and the [FAIR Data Principles](https://www.rd-alliance.org/group/fair-data-maturity-model-wg/outcomes/fair-data-maturity-model-specification-and-guidelines).


## List of indicators
**Errors**

- date_format -- Conformity of date formats to ISO 8601, see [explanation](#date_format)
- number_format -- number formats do not conform to a standard, see [explanation](#number_format)
- null_values -- mark null values explicitly as such
- wrong_content_type -- content type is not text/csv
- ragged_rows -- row has a different number of columns (than the first row in the file)
- blank_rows -- completely empty row, e.g. blank line or a line where all column values are empty
- stray_quote -- missing or stray quote
- whitespace -- a quoted column has leading or trailing whitespace
- line_breaks -- line breaks were inconsistent or incorrectly specified

**Warnings**

- delimiter -- only a semicolon should be used as delimiter
- no_encoding -- the Content-Type header returned in the HTTP request does not have a charset parameter, see [explanation](#encoding)
- encoding -- the character set is not UTF-8, see [explanation](#encoding)
- no_content_type -- file is being served without a Content-Type header
- excel -- no Content-Type header and the file extension is .xls
- check_options -- CSV file appears to contain only a single column
- inconsistent_values -- inconsistent values in the same column. Reported if <90% of values seem to have same data type (either numeric or alphanumeric including punctuation)
  - -> detected data types: date, integer, numeric, boolean and text
- empty_column_name -- a column in the CSV header has an empty name
- duplicate_column_name -- a column in the CSV header has a duplicate name
- title_row -- if there appears to be a title field in the first row of the CSV

**Information**

- nonrfc_line_breaks -- uses non-CRLF line breaks, so doesn't conform to RFC4180.
- assumed_header -- the validator has assumed that a header is present

## Explanations

### number_format

This indicator comes from the DQG, in which the following is stated:

>To avoid the unintended interpretation of a comma separating a whole
number from a decimal, a dot should be used instead.
When dealing with large numbers, sometimes a thousand separator is used, for example
a dot or white space. Again, this can lead to misinterpretation – especially when the
data is being processed automatically – and might mean the user has to clean the data
before they can reuse it. Thousand separators should therefore not be used.


- A single `.` in a number could indicate a decimal separator (correct) or a thousand separator (incorrect)
- These cases cannot be distinguished, so I will not create an Error for this. Which could lead to a false negative
  - it is possible to detect the intended usage when there is a single Zero before the dot. (e.g. 0.52) In this case the dot is clearly used as a decimal separator. However, as this is the correct usage, no Error will be generated in this case, too
- a `,`is incorrect no matter where. This will be detected internally as thousand separator and the error indicator will always be shown for that.

### date_format

There are Date formats that are not distinguishable from normal numbers, e.g. yyyyMMdd -> 20220721. These will not be treated as dates but this means that this will not throw an error, even though it is not ISO 8601 and would normally trigger an Error.

### encoding

Regarding the following indicators:

> no_encoding -- the Content-Type header returned in the HTTP request does not have a charset parameter

> encoding -- the character set is not UTF-8

This will only be checked, if there is a Content-Type header to check. If the Content-Type header is missing completely, there is another warning: `no_content_type `. Similarly, the `encoding` indicator will only be checked, if there is a charset parameter in the Content-Type.

