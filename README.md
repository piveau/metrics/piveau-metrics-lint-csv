# Piveau Metrics Lint CSV


This library is used to lint CSV files. The result can be retrieved as JSON or as a DQV Graph. 

A list with descriptions of all indicators can be found [here](Indicators.md).

## Table of Contents

1. [Build](#build)
1. [Use](#use)
1. [Docker](#docker)
1. [Maintainer](#maintainer)
1. [License](#license)



## Build
Requirements:
* Git
* Maven 3
* Java 17


```bash
$ git clone <gitrepouri> ...
$ cd piveau-metrics-lint-csv
$ mvn package
```

## Test

```bash
$ mvn clean test
```


## Use

Add repository to your project pom file:
```xml
<repository>
    <id>paca</id>
    <name>paca</name>
    <url>https://paca.fokus.fraunhofer.de/repository/maven-public/</url>
</repository>
```

Add dependency to your project pom file:
```xml
<dependency>
    <groupId>de.piveau.metrics.linter.csv</groupId>
    <artifactId>piveau-metrics-lint-csv</artifactId>
    <version>0.1.1-SNAPSHOT</version>
</dependency>
```



### Linting API

```java
CSVLinter linter = new CSVLinter("/path/to/file.csv", "text/csv; charset=utf-8");
Result result = linter.validate();

if (result.isPassed()) {
    System.out.println("File is valid");
} else {
    System.out.println("File is not valid");
}
```


## Projects using this library

This library is used in the piveau.io platform to lint CSV files that are contained in datasets. The result is used by the metrics component.


## Maintainer

## License

[Apache License, Version 2.0](LICENSE.md)

