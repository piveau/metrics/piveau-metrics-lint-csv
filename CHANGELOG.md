# ChangeLog

# Unreleased

**Fixed:**
* Linked Data conversion methods
* column numbering in the error messages
* CSV class not checking metadata by default
* Separator detection bottelneck

**Added:**
* delimiter detection
* ability to specify the uri of the base resource for the graph
* Indicator titles
* Support for configurable limited result output
* Support for configurable limited result checking

**Changed:**
* Validation will skip CSV linting if Metadata is not valid
* Regex for detecting date formats reduced to one
* error message for Semicolon error

## 0.1.1 (2022-08-23)

* Fixed: specific case with quoted fields and comma as not recognized delimiter

## 0.1.0 (2022-08-16)

Initial release
