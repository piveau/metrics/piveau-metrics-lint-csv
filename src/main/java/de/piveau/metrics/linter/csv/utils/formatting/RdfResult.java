package de.piveau.metrics.linter.csv.utils.formatting;


import de.piveau.metrics.linter.csv.indicators.ErrorIndicator;
import de.piveau.metrics.linter.csv.indicators.Indicator;
import de.piveau.metrics.linter.csv.indicators.InformationIndicator;
import de.piveau.metrics.linter.csv.indicators.WarningIndicator;
import de.piveau.metrics.linter.csv.validation.Result;
import io.piveau.vocabularies.vocabulary.DQV;
import io.piveau.vocabularies.vocabulary.PV;
import io.piveau.vocabularies.vocabulary.SHACL;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.OA;

import java.util.HashMap;
import java.util.Map;

public class RdfResult implements ResultFormat {

    protected final Result result;
    protected final Model model = ModelFactory.createDefaultModel();

    protected final Map<Indicator, Resource> resources = new HashMap<>() {
        {
            put(ErrorIndicator.DATE_FORMAT, PV.dateFormat);
            put(ErrorIndicator.NUMBER_FORMAT, PV.numberFormat);
            put(WarningIndicator.DELIMITER, PV.delimiter);
            put(ErrorIndicator.NULL_VALUES, PV.nullValues);
            put(ErrorIndicator.WRONG_CONTENT_TYPE, PV.wrongContentType);
            put(ErrorIndicator.RAGGED_ROWS, PV.raggedRows);
            put(ErrorIndicator.BLANK_ROWS, PV.blankRows);
            put(ErrorIndicator.INVALID_ENCODING, PV.invalidEncoding);
            put(ErrorIndicator.NOT_FOUND, PV.notFound);
            put(ErrorIndicator.STRAY_QUOTE, PV.strayQuote);
            put(ErrorIndicator.WHITESPACE, PV.whitespace);
            put(ErrorIndicator.LINE_BREAKS, PV.lineBreaks);

            put(WarningIndicator.NO_ENCODING, PV.noEncoding);
            put(WarningIndicator.ENCODING, PV.encoding);
            put(WarningIndicator.NO_CONTENT_TYPE, PV.noContentType);
            put(WarningIndicator.EXCEL, PV.excel);
            put(WarningIndicator.CHECK_OPTIONS, PV.checkOptions);
            put(WarningIndicator.INCONSISTENT_VALUES, PV.inconsistentValues);
            put(WarningIndicator.EMPTY_COLUMN_NAME, PV.emptyColumnName);
            put(WarningIndicator.DUPLICATE_COLUMN_NAME, PV.duplicateColumnName);
            put(WarningIndicator.TITLE_ROW, PV.titleRow);

            put(InformationIndicator.NONRFC_LINE_BREAKS, PV.nonrfcLineBreaks);
            put(InformationIndicator.ASSUMED_HEADER, PV.assumedHeader);
        }
    };
    protected final Resource resource;


    public static class Builder {

        protected RdfResult rdfResult;
        private Result result;
        private String resourceUri = "urn:CSV";


        public Builder(Result result) {
            this.result = result;

        }

        public Builder(Result result, String resourceUri) {
            this.result = result;
            this.resourceUri = resourceUri;

        }

        public Builder(RdfResult rdfResult) {
            this.rdfResult = rdfResult;
        }

        public Builder setResourceUri(String resourceUri) {
            this.resourceUri = resourceUri;
            return this;
        }

        public RdfResult build() {
            rdfResult = new RdfResult(result, resourceUri);
            return this.rdfResult.generate();
        }


    }


    protected RdfResult(Result result) {
        resource = model.createResource("urn:CSV");

        this.result = result;

    }

    protected RdfResult(Result result, String resourceUri) {
        resource = model.createResource(resourceUri);

        this.result = result;

    }

    public Model getModel() {
        return model;
    }


    private RdfResult generate() {


        for (Result.ResultItem item : result.getIndicators(Indicator.IndicatorType.WARNING)) {
            addWarning(model, item);

        }
        for (Result.ResultItem item : result.getIndicators(Indicator.IndicatorType.ERROR)) {
            addError(model, item);
        }
        for (Result.ResultItem item : result.getIndicators(Indicator.IndicatorType.INFORMATION)) {
            addInformation(model, item);
        }


        Resource body = model.createResource(PV.csvIndicator);
        body.addLiteral(PV.rowNumber, result.getRowCount());
        body.addLiteral(PV.passed, result.isPassed());


        Resource annotation = model.createResource(DQV.QualityAnnotation);

        annotation.addProperty(OA.motivatedBy, OA.assessing);
        annotation.addProperty(OA.hasBody, body);


        model.add(resource, DQV.hasQualityAnnotation, annotation);
        return this;
    }

    void addError(Model report, Result.ResultItem item) {
        Resource body = report.createResource(PV.csvError);
        addCsvIndicatorAnnotation(report, body, item);
    }

    void addWarning(Model report, Result.ResultItem item) {
        Resource body = report.createResource(PV.csvWarning);
        addCsvIndicatorAnnotation(report, body, item);
    }

    void addInformation(Model report, Result.ResultItem item) {
        Resource body = report.createResource(PV.csvInformation);
        addCsvIndicatorAnnotation(report, body, item);
    }

    private void addCsvIndicatorAnnotation(Model report, Resource body, Result.ResultItem item) {

        body.addProperty(SHACL.result, resources.get(item.indicator()));
        body.addLiteral(SHACL.resultMessage, item.getMessage());
        body.addLiteral(DC.title, item.indicator().getTitle());
        body.addLiteral(PV.rowNumber, item.row());
        body.addLiteral(PV.columnNumber, item.column());
        body.addLiteral(DC.identifier, item.getName());


        Resource annotation = report.createResource(DQV.QualityAnnotation);


        annotation.addProperty(OA.motivatedBy, OA.assessing);
        annotation.addProperty(OA.hasBody, body);


        report.add(resource, DQV.hasQualityAnnotation, annotation);
    }


}
