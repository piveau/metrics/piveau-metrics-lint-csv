package de.piveau.metrics.linter.csv.utils.formatting;

import de.piveau.metrics.linter.csv.indicators.Indicator;
import de.piveau.metrics.linter.csv.validation.Result;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;


public class JsonResult {

    private final Result result;
    private final JsonObject json = new JsonObject();

    public JsonResult(Result result) {
        this.result = result;
        generate();
    }

    public JsonObject getJson() {
        return json;
    }

    private void generate() {

        json.put("passed", result.isPassed());
        json.put("itemCount", result.count());
        json.put("rowCount", result.getRowCount());

        // add errors
        JsonObject errors = new JsonObject();
        errors.put("count", result.count(Indicator.IndicatorType.ERROR));

        JsonArray errorArray = new JsonArray();

        for (Result.ResultItem item : result.getIndicators(Indicator.IndicatorType.ERROR)) {
            addItem(errorArray, item);
        }

        errors.put("items", errorArray);
        json.put("errors", errors);

        // add warnings
        JsonObject warnings = new JsonObject();
        warnings.put("count", result.count(Indicator.IndicatorType.WARNING));

        JsonArray warningArray = new JsonArray();

        for (Result.ResultItem item : result.getIndicators(Indicator.IndicatorType.WARNING)) {
            addItem(warningArray, item);
        }

        warnings.put("items", warningArray);
        json.put("warnings", warnings);

        // add infos
        JsonObject info = new JsonObject();
        info.put("count", result.count(Indicator.IndicatorType.INFORMATION));

        JsonArray infoArray = new JsonArray();

        for (Result.ResultItem item : result.getIndicators(Indicator.IndicatorType.INFORMATION)) {
            addItem(infoArray, item);
        }

        info.put("items", infoArray);
        json.put("info", info);


    }

    private void addItem(JsonArray array, Result.ResultItem item) {
        JsonObject jsonItem = new JsonObject();
        jsonItem.put("title", item.indicator().getTitle());
        jsonItem.put("indicator", item.getName());
        jsonItem.put("message", item.getMessage());
        jsonItem.put("row", item.row());
        jsonItem.put("column", item.column());
        jsonItem.put("columnName", item.columnName());

        array.add(jsonItem);
    }

}
