package de.piveau.metrics.linter.csv.indicators;

public enum InformationIndicator implements Indicator {

    NONRFC_LINE_BREAKS("nonrfc_line_breaks", "Uses non-CRLF line breaks, so doesn't conform to RFC4180", "Non-RFC Line Breaks"),
    ASSUMED_HEADER("assumed_header", "The validator has assumed that a header is present", "Assumed Header");

    private final String message;
    private final String name;
    private final String titel;


    InformationIndicator(String name, String message, String titel) {
        this.name = name;
        this.message = message;
        this.titel = titel;
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }

    public String getTitle() {
        return titel;
    }

    public IndicatorType getType() {
        return IndicatorType.INFORMATION;
    }


}
