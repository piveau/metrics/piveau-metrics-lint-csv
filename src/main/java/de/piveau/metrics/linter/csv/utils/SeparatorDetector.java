package de.piveau.metrics.linter.csv.utils;

import java.util.*;

public class SeparatorDetector {

    Boolean quoted = false;
    Boolean escaped = false;
    Set<Character> separators;

    List<Character> detectedSeparators = new ArrayList<>();
    Map<Character, Integer> separatorCount = new HashMap<>();

    public SeparatorDetector(Set<Character> separators) {
        this.separators = separators;
    }

    public SeparatorDetector() {
        this.separators = SEPARATORS;
    }


    public final Set<Character> SEPARATORS = Set.of(',', ';', '\t', '|', ':', '#', '.');

    public void detect(String line) {

        for (int i = 0; i < line.length(); i++) {
            char c = line.charAt(i);

            if (c == '"') {
                if (!escaped) {
                    quoted = !quoted;
                }
                escaped = false;
            } else if (c == '\\') {
                escaped = !escaped;
            } else if (separators.contains(c) && !quoted && !escaped) {
                incrementCount(c);
            } else {
                escaped = false;
            }
        }
    }


    private void incrementCount(Character c) {
        if (separatorCount.containsKey(c)) {
            separatorCount.replace(c, separatorCount.get(c) + 1);
        } else {
            separatorCount.put(c, 1);

        }
    }

    public Character getMostFrequentSeparator() {

        try {
            Optional<Map.Entry<Character, Integer>> maxEntry = separatorCount.entrySet().stream().max(Map.Entry.comparingByValue());
            return maxEntry.map(Map.Entry::getKey).orElse(null);

        } catch (Exception e) {
            return null;
        }


    }


}
