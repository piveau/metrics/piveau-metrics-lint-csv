package de.piveau.metrics.linter.csv.indicators;

public enum WarningIndicator implements Indicator {

    UNKNOWN_WARNING("unknown_warning", "Unknown warning", "Unknown Warning"),
    NO_ENCODING("no_encoding", "The Content-Type header returned in the HTTP request does not have a charset parameter", "No Encoding"),
    ENCODING("encoding", "The character set is not UTF-8", "Encoding"),
    NO_CONTENT_TYPE("no_content_type", "File is being served without a Content-Type header", "No Content-Type"),
    EXCEL("excel", "No Content-Type header and the file extension is .xls", "Excel"),
    CHECK_OPTIONS("check_options", "CSV file appears to contain only a single column", "Check Options"),
    INCONSISTENT_VALUES("inconsistent_values", "Inconsistent values in the same column. Reported if <90% of values seem to have same data type (either numeric or alphanumeric including punctuation)", "Inconsistent Values"),
    EMPTY_COLUMN_NAME("empty_column_name", "A column in the CSV header has an empty name", "Empty Column Name"),
    DUPLICATE_COLUMN_NAME("duplicate_column_name", "A column in the CSV header has a duplicate name", "Duplicate Column Name"),
    TITLE_ROW("title_row", "There appears to be a title field in the first row of the CSV", "Title Row"),
    DELIMITER("delimiter", "A semicolon should be used as delimiter", "Delimiter");


    private final String message;
    private final String name;

    private final String titel;


    WarningIndicator(String name, String message, String titel) {
        this.name = name;
        this.message = message;
        this.titel = titel;
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }

    public String getTitle() {
        return titel;
    }

    public IndicatorType getType() {
        return IndicatorType.WARNING;
    }

}
