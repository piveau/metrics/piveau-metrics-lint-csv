package de.piveau.metrics.linter.csv.validation;

import java.util.Objects;

public class MetadataBuilder {


    /**
     * the default Metadata builder
     */
    public MetadataBuilder() {
        result = new Result();
    }

    private Result result;
    private String fileName;
    private String contentType = null;

    /**
     * Set the result object.
     * @param result The result object.
     * @return this
     */
    public MetadataBuilder setResult(Result result) {
        this.result = result;
        return this;
    }

    /**
     * Sets the file name.
     * @param fileName The file name.
     * @return this
     */
    public MetadataBuilder setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    /**
     * Sets the content type of the file.
     * @param contentType The content type of the file.
     * @return this
     */
    public MetadataBuilder setContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }


    /**
     * Builds a new {@link Metadata} instance.
     * @return {@link Metadata} instance.
     */
    public Metadata build() {
        Objects.requireNonNull(fileName, "fileName must not be null");

        return new Metadata(result, fileName, contentType);


    }


}
