package de.piveau.metrics.linter.csv.validation;


import de.piveau.metrics.linter.csv.indicators.ErrorIndicator;
import de.piveau.metrics.linter.csv.indicators.WarningIndicator;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class creates a {@link CSV} object with given information.
 * The CSV object has a parser and can be used to validate a CSV file.
 */
public class CSVBuilder {


    private Result result;
    private String charset = "UTF-8";


    /**
     * Create a new CSVBuilder instance with a specified Result.
     * @param result The result object.
     */
    public CSVBuilder(Result result) {
        this.result = result;
    }

    /**
     * Create a new CSVBuilder instance with the default result
     */
    public CSVBuilder() {
        result = new Result();
    }



    /**
     * Setting the result object.
     * @param result The result object.
     * @return The CSVBuilder object.
     */
    public CSVBuilder setResult(Result result) {
        this.result = result;
        return this;
    }


    /**
     * Creates a {@link CSV} object using the information extracted in the {@link Metadata}.
     * @param metadata the {@link Metadata} object to use
     * @return {@link CSV} object with a {@link CSVParser}.
     * @throws IOException if an I/O error occurs
     */
    public CSV buildFromMetadataAndParse(Metadata metadata, String delimiter) throws IOException {
        this.result = metadata.getResult();
        String fileName = metadata.getFileName();


        // Possible Syntax:
        // text/csv;charset=UTF-8
        //multipart/form-data; boundary=something

        String[] cts = metadata.getContentType().split(";");
        if (cts.length == 2) {
            String[] ct = cts[1].split("=");

            if (ct.length == 2) {
                this.charset = ct[1];
            }

        }

        if(metadata.getContentType()!= null && !metadata.getContentType().isBlank()) {
            this.charset = metadata.getContentType().split(";")[1].split("=")[1];
        }


        return buildAndParseCSVFile(fileName, charset,delimiter);
    }



    /**
     * Creates a {@link CSV} object from a file with the given name and charset.
     * This also creates a {@link CSVParser} for the file.
     *
     * @param file      name of the file
     * @param cs        charset of the file
     * @param delimiter delimiter of the file
     * @return {@link CSV} object with a {@link CSVParser}.
     * @throws IOException if an I/O error occurs
     */
    public CSV buildAndParseCSVFile(String file, String cs, String delimiter) throws IOException, NullPointerException {
        Objects.requireNonNull(file, "file name must not be null");

        Objects.requireNonNull(delimiter, "delimiter must not be null");


        if (file.isBlank()) {
            throw new NullPointerException("file name must not be null");
        }


        if (!delimiter.matches("^;$")) {
            result.addIndicator(WarningIndicator.DELIMITER, -1L, -1L, "");
        }

        try {
            Charset cs_charset = Charset.forName(charset);
            try {
                cs_charset = Charset.forName(cs);
            } catch (Exception ignored) {

            }

            CSVParser parser = CSVParser.parse(
                    Path.of(file),
                    cs_charset,
                    CSVFormat.Builder.create()
                            .setAllowDuplicateHeaderNames(!result.checkingIndicator(WarningIndicator.DUPLICATE_COLUMN_NAME))
                            .setAllowMissingColumnNames(!result.checkingIndicator(WarningIndicator.EMPTY_COLUMN_NAME))
                            .setHeader()
                            .setDelimiter(delimiter).setIgnoreEmptyLines(!result.checkingIndicator(ErrorIndicator.BLANK_ROWS))
                            .setIgnoreSurroundingSpaces(false)


                            .setTrim(false).build()
            );
            return new CSV(parser, result);
        } catch (IllegalArgumentException e) {

            if (e.getMessage().startsWith("A header name is missing in")) {
                String headers = e.getMessage().substring(0, e.getMessage().length() - 1).replaceFirst("A header name is missing in \\[", "");
                String[] header = headers.split(",");
                // check for the position of the empty header in the header array
                for (int i = 0; i < header.length; i++) {
                    if (header[i].isBlank()) {
                        result.addIndicator(WarningIndicator.EMPTY_COLUMN_NAME, 1L, (long) i + 1, "");
                    }
                }


            }


            //we have the error message, now we can run it without the checks
            return parseCSVFileWithoutValidation(file, charset, delimiter);
        } catch (IOException e) {
            //\(line (\d+)\).*

            Pattern pattern =
                    Pattern.compile("/\\(line (\\d+)\\) invalid char between encapsulated token and delimiter/");

            Matcher matcher =
                    pattern.matcher(e.getMessage());
            if (matcher.find()) {
                Long row = Long.parseLong(matcher.group(1));
                result.addIndicator(ErrorIndicator.STRAY_QUOTE, row, -1L, "");
                result.addIndicator(WarningIndicator.DELIMITER, row, -1L, "");
            } else {
                result.addIndicator(ErrorIndicator.STRAY_QUOTE, -1L, -1L, "");
            }

            return new CSV(null, result);
        }

    }

    /**
     * Creates a {@link CSV} object from a file with the given name. Charset will be UTF-8 and delimiter will be ";".
     * This also creates a parser for the file.
     * @param file name of the file

     * @return {@link CSV} object with a {@link CSVParser}.
     * @throws IOException if an I/O error occurs
     */
    public CSV buildAndParseCSVFile(String file) throws IOException {
        return buildAndParseCSVFile(file, charset, ";");
    }

    /**
     * Creates a {@link CSV} object from a file with the given name and {@link Charset}. The delimiter will be ";".
     * This also creates a parser for the file.
     * @param file name of the file
     * @param cs charset of the file
     * @return {@link CSV} object with a {@link CSVParser}.
     * @throws IOException if an I/O error occurs
     */
    public CSV buildAndParseCSVFile(String file, Charset cs) throws IOException {
        return buildAndParseCSVFile(file, cs.name(), ";");
    }

    /**
     * Creates a {@link CSV} object from a file with the given name and delimiter. Charset will be UTF-8.
     * This also creates a parser for the file.
     * @param file name of the file
     * @param delimiter delimiter of the file
     * @return {@link CSV} object with a {@link CSVParser}.
     * @throws IOException if an I/O error occurs
     */
    public CSV buildAndParseCSVFile(String file, String delimiter) throws IOException {
        return buildAndParseCSVFile(file, charset, delimiter);
    }

    /**
     * This method is used when the CSV file is not valid and we want to parse it without the checks
     * @param file the file to parse
     * @param charset the charset of the file
     * @param delimiter the delimiter of the file
     * @return  a new {@link CSV} instance with a {@link CSVParser}.
     * @throws IOException if an I/O error occurs
     *
     */
    private CSV parseCSVFileWithoutValidation(String file, String charset, String delimiter) throws IOException {
        Objects.requireNonNull(file);
        Objects.requireNonNull(charset);
        Objects.requireNonNull(delimiter);


            CSVParser parser = CSVParser.parse(
                    Path.of(file),
                    Charset.forName(charset),
                    CSVFormat.Builder.create()
                            .setAllowDuplicateHeaderNames(true)
                            .setAllowMissingColumnNames(true)
                            .setHeader()
                            .setDelimiter(delimiter).setIgnoreEmptyLines(!result.checkingIndicator(ErrorIndicator.BLANK_ROWS))
                            .setIgnoreSurroundingSpaces(false)

                            .setTrim(false).build()
            );
            return new CSV(parser, result);

    }


}
