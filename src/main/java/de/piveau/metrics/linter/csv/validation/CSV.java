package de.piveau.metrics.linter.csv.validation;


import de.piveau.metrics.linter.csv.indicators.ErrorIndicator;
import de.piveau.metrics.linter.csv.indicators.InformationIndicator;
import de.piveau.metrics.linter.csv.indicators.WarningIndicator;
import de.piveau.metrics.linter.csv.utils.DateUtils;
import de.piveau.metrics.linter.csv.utils.NumberUtils;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * This class uses the {@link CSVParser} to validate a CSV file and write the results to a {@link Result}.
 */
public class CSV {

    //private final Logger log = LoggerFactory.getLogger(getClass());

    private CSVParser parser = null;
    private final Result result;
    private Map<String, List<String>> schema = null;
    private final List<Integer> columnCounts = new ArrayList<>();
    private Boolean oneColumn = true;
    private Iterator<CSVRecord> iterator = null;

    private boolean cont = true;

    protected CSV(CSVParser parser, Result result) {


        result.addIndicator(InformationIndicator.ASSUMED_HEADER, 1L, -1L, "");

        if(parser != null) {
            this.parser = parser;
            columnCounts.add(parser.getHeaderNames().size());
            if (columnCounts.get(0) > 1) {
                oneColumn = false;
            }

            //System.out.println("Headermap: " + parser.getHeaderNames().size());

            schema = parser.getHeaderNames().stream().filter(Objects::nonNull).distinct().collect(Collectors.toMap(Function.identity(), (i) -> new ArrayList<>()));

            if (result.checkBelowLimit() && result.checkingIndicator(WarningIndicator.EMPTY_COLUMN_NAME) && !result.contains(WarningIndicator.EMPTY_COLUMN_NAME)) {

                for (int i = 0; i < schema.keySet().size(); i++) {
                    if (schema.keySet().stream().toList().get(i).isEmpty()) {
                        cont = result.addIndicator(WarningIndicator.EMPTY_COLUMN_NAME, 1L, (long) i + 1, "");
                        if (!cont) {
                            break;
                        }
                    }
                }
            }

            if (result.checkBelowLimit() && result.checkingIndicator(WarningIndicator.DUPLICATE_COLUMN_NAME)) {
                for (String columnName : parser.getHeaderNames()) {
                    if (parser.getHeaderNames().stream().filter(l -> l.equals(columnName)).count() > 1) {
                        cont = result.addIndicator(WarningIndicator.DUPLICATE_COLUMN_NAME, 1L, Long.valueOf(parser.getHeaderMap().get(columnName)) + 1, columnName);
                        if (!cont) {
                            break;
                        }
                    }
                }
            }


            iterator = parser.iterator();
        }

        this.result = result;
    }

//TODO: best JSON Library?


    /**
     * Validate the csv file row by row and write the results to the {@link Result}.
     */
    public void validate() {


        if(parser == null) {
            return;
        }

        long row = 1;

        try {
            while (iterator.hasNext() && cont) {
                CSVRecord record = iterator.next();


                if (record.size() > 1) {
                    oneColumn = false;
                }
                columnCounts.add(record.size());

                //if the row is empty, skip it
                //an empty row also has a size of 1, so we need to check if the row is empty
                if (record.size() == 1 && record.get(0).isBlank()) {
                    cont = result.addIndicator(ErrorIndicator.BLANK_ROWS, record.getParser().getCurrentLineNumber(), -1L, "");

                } else {
                    if (record.size() != parser.getHeaderNames().size()) {
                        cont = result.addIndicator(ErrorIndicator.RAGGED_ROWS, record.getParser().getCurrentLineNumber(), -1L, "");

                    }
                    parseRow(record);
                }

                row++;

            }
        } catch (IllegalStateException | UncheckedIOException e) {
            if (e.getMessage().contains("invalid char between encapsulated token and delimiter")) {
                System.out.println(e.getMessage());
                cont = result.addIndicator(ErrorIndicator.STRAY_QUOTE, row + 1, -1L, "");
            } else if (e.getMessage().contains("invalid char")) {
                System.out.println(e.getMessage());
                System.out.println("Line: " + parser.getCurrentLineNumber());
                System.out.println("row: " + row);
                cont = result.addIndicator(ErrorIndicator.INVALID_ENCODING, row, -1L, "");
            } else {
                throw e;
            }
        }

        result.setRowCount(row);

        if (!cont) {
            return;
        }
        // check if
        schema.forEach((k, v) -> {

            Map<String, Long> map = v.stream().filter((it) -> !it.equals("NULL")).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            int count = map.values().stream().mapToInt(Long::intValue).sum();

            map.forEach((k1, v1) -> {
                double percent = (double) v1 / (double) count;
                //System.out.println(k + ":" + k1 + " " + v1 + "/" +count +"=" + v1 / (double)count);
                if (percent < 0.9 && percent > 0.1) {
                    //System.out.println("Inconsistent: " + k + ":" + k1 + " " + v1 + "/" +count +"=" + v1 / (double)count);
                    cont = result.addIndicator(WarningIndicator.INCONSISTENT_VALUES, -1L, Long.valueOf(parser.getHeaderMap().get(k)) + 1, k);

                }

            });

        });

        if (!cont) {
            return;
        }

        int columSum = columnCounts.stream().mapToInt(Integer::intValue).sum();

        if (result.checkBelowLimit() && result.checkingIndicator(WarningIndicator.TITLE_ROW) && columnCounts.get(0) < (columSum / columnCounts.size())) {
            cont = result.addIndicator(WarningIndicator.TITLE_ROW, -1L, -1L, "");

        }

        if (oneColumn) {
            cont = result.addIndicator(WarningIndicator.CHECK_OPTIONS, -1L, 1L, "");
        }
    }


    /**
     * Parses a row and adds the indicators to the result
     *
     * @param record the row to parse
     */
    private void parseRow(CSVRecord record) {


        if (result.checkingIndicator(ErrorIndicator.BLANK_ROWS)) {
            long nullcells = record.toList().stream()
                    .filter((it) -> it.isBlank() || it.equalsIgnoreCase("null") || it.equalsIgnoreCase("na")).count();

            if (nullcells == record.size()) {
                cont = result.addIndicator(ErrorIndicator.BLANK_ROWS, record.getParser().getCurrentLineNumber(), -1L, "");
            }
        }

        record.toMap().entrySet().iterator().forEachRemaining((e) -> {
            String header = e.getKey();
            String value = e.getValue();
            if (!cont) {
                return;
            }

            if (result.checkingIndicator(ErrorIndicator.WHITESPACE)) {
                if (!value.equals(value.trim())) {
                    cont = result.addIndicator(ErrorIndicator.WHITESPACE, record.getParser().getCurrentLineNumber(), (long) record.getParser().getHeaderNames().indexOf(header) + 1, header);
                }
            }
            /*if (header == null) {
                System.out.println("Header is null");

            }*/

            if (value != null && !value.isBlank() && !value.trim().equalsIgnoreCase("NULL") && !value.trim().equalsIgnoreCase("NA")) {
                String trimmed = value.trim();
                //System.out.println("Trimmed != value: |" + trimmed + "| != |" + value+"|");

                schema.get(header).add(detectFormat(trimmed, parser.getCurrentLineNumber(), header));


            } else {
                if (value == null || value.isBlank()) {
                    cont = result.addIndicator(ErrorIndicator.NULL_VALUES, parser.getCurrentLineNumber(), Long.valueOf(parser.getHeaderMap().get(header)) + 1, header);
                }
                schema.get(header).add("NULL");

            }

        });


    }

    /**
     * Detects the format of a value to decide if it is a date, a number or a string
     * Wrong format will be detected and added to the result
     * @param value the cell value
     * @param row the row number
     * @param column the column name
     * @return the detected format
     */
    private String detectFormat(String value, Long row, String column) {

        if (value.toLowerCase().matches("^true|false$")) {
            return "BOOLEAN";
        }

        NumberUtils.NumberType numberType = NumberUtils.getNumberType(value);
        if (numberType != NumberUtils.NumberType.UNKNOWN) {
            if (!NumberUtils.followsDQGRecommendation(value)) {
                cont = result.addIndicator(ErrorIndicator.NUMBER_FORMAT, row, Long.valueOf(parser.getHeaderMap().get(column)) + 1, column);
            }
            return numberType == NumberUtils.NumberType.INTEGER ? "INTEGER" : "NUMERIC";
        }

        if (isDate(value, row, column)) {
            return "DATE";
        }
        return "TEXT";
    }


    /**
     * Checks if the value is a decimal number.
     * If the value can be detected as a decimal number, it will be checked if it is a valid number.
     * If the format is invalid, an indicator will be added to the result
     * @param value the cell value
     * @param row the row number
     * @param column the column name
     * @return true if the value is detected as a decimal number, false otherwise
     */
    Boolean isNumeric(String value, Long row, String column) {
        NumberUtils.NumberType numberType = NumberUtils.getNumberType(value);

        if (numberType == NumberUtils.NumberType.DECIMAL) {
            if (!NumberUtils.followsDQGRecommendation(value)) {
                cont = result.addIndicator(ErrorIndicator.NUMBER_FORMAT, row, Long.valueOf(parser.getHeaderMap().get(column)) + 1, column);
            }
            return true;
        }
        return false;
    }

    /**
     * Checks if the value is an integer.
     * If the value can be detected as an integer, it will be checked if it is a valid number.
     * If the format is invalid, an indicator will be added to the result
     * @param value the cell value
     * @param row the row number
     * @param column the column name
     * @return true if the value is detected as an integer
     */
    Boolean isInteger(String value, Long row, String column) {
        NumberUtils.NumberType numberType = NumberUtils.getNumberType(value);

        if (numberType == NumberUtils.NumberType.INTEGER) {
            if (!NumberUtils.followsDQGRecommendation(value)) {
                cont = result.addIndicator(ErrorIndicator.NUMBER_FORMAT, row, Long.valueOf(parser.getHeaderMap().get(column)) + 1, column);
            }
            return true;
        }
        return false;
    }

    /**
     * Checks if the value is a date.
     * If the value can be detected as a date, it will be checked if it is a valid date.
     * If the format is invalid, an indicator will be added to the result
     * @param value the cell value
     * @param row the row number
     * @param column the column name
     * @return true if the value is detected as a date
     */
    Boolean isDate(String value, Long row, String column) {
        if (DateUtils.isISODateFormat(value)) {
            return true;
        } else if (DateUtils.isDateTime(value)) {
            cont = result.addIndicator(ErrorIndicator.DATE_FORMAT, row, Long.valueOf(parser.getHeaderMap().get(column)) + 1, column);
            return true;
        } else {
            return false;
        }

    }


}