package de.piveau.metrics.linter.csv;

import de.piveau.metrics.linter.csv.indicators.Indicator;
import de.piveau.metrics.linter.csv.validation.*;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * This class is for coordinating and initializing the checks
 */
public class CSVLinter {
    private final String filename;
    private final String contentType;
    private String delimiter;

    private Result result = new Result();
    private boolean checkMeta = true, checkCsv = true;
    private String charset = "UTF-8";
    private final Integer limit;

    /**
     * Create a new CSVLinter instance with a specified filename and content type.
     *
     * @param filename    The filename of the CSV file.
     * @param contentType The content type of the CSV file
     * @param delimiter   the delimiter to use
     */
    public CSVLinter(String filename, String contentType, String delimiter, Integer limit) {
        this.filename = filename;
        this.contentType = contentType;
        this.delimiter = delimiter;
        this.limit = limit;
    }


    /**
     * Create a new CSVLinter instance with a specified filename and content type.
     *
     * @param filename    The filename of the CSV file.
     * @param contentType The content type of the CSV file
     * @param delimiter   the delimiter to use
     */
    public CSVLinter(String filename, String contentType, String delimiter) {
        this(filename, contentType, delimiter, null);
    }

    /**
     * Like {@link #CSVLinter(String, String, String, Integer)} but with a default delimiter of `;`
     *
     * @param filename    the name of the local file to lint
     * @param contentType the contenttype of the file
     * @param limit       the limit of rows to check
     */
    public CSVLinter(String filename, String contentType, Integer limit) {
        this(filename, contentType, null, limit);
    }

    /**
     * Like {@link #CSVLinter(String, String, String)} but with a default delimiter of `;`
     *
     * @param filename    the name of the local file to lint
     * @param contentType the contenttype of the file
     */
    public CSVLinter(String filename, String contentType) {
        this(filename, contentType, null, null);
    }

    /**
     * If a specific set of indicators should be checked, this can be set here
     * @param indicators the set of indicators that should be checked
     * @return this for chaining
     */
    public CSVLinter setIndicators(Set<Indicator> indicators) {
        result = new Result(indicators);
        return this;
    }

    /**
     * If only one single indicator should be checked, that can be set here. This operation removes all other indicators from the set.
     *
     * @param indicator the single indicator that should be checked
     * @return this for chaining
     */
    public CSVLinter setSingleIndicator(Indicator indicator){
        result = new Result(new HashSet<>(Collections.singleton(indicator)));
        return this;
    }

    /**
     * @param check if the metadata should be checked
     * @return this for chaining
     */
    public CSVLinter setCheckMetadata(boolean check) {
        checkMeta = check;
        return this;
    }

    /**
     * @param check if the csv content should be checked
     * @return this for chaining
     */
    public CSVLinter setCheckCSVContent(boolean check) {
        checkCsv = check;
        return this;
    }

    public Result validate() throws IOException {
        if (limit != null && limit > 0) {
            result.setLimit(limit);
        }

        if (checkMeta) {

            Metadata metadata = new MetadataBuilder()
                    .setResult(result)
                    .setFileName(filename)
                    .setContentType(contentType)
                    .build();

            result = metadata.checkContentType().checkFile().getResult();
            charset = metadata.getCharset();
            if (delimiter == null) {
                delimiter = metadata.getDetectedSeparator().toString();
            }
        }

        if (checkCsv && result.isPassed()) {
            CSVBuilder builder = new CSVBuilder();

            if (delimiter == null) {
                delimiter = ";";
            }

            CSV csv = builder.setResult(result).buildAndParseCSVFile(filename, charset, delimiter);

            if (csv != null) {
                csv.validate();
            }
        }
        return result;


    }


}
