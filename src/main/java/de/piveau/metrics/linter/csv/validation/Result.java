package de.piveau.metrics.linter.csv.validation;


import de.piveau.metrics.linter.csv.indicators.ErrorIndicator;
import de.piveau.metrics.linter.csv.indicators.Indicator;
import de.piveau.metrics.linter.csv.indicators.WarningIndicator;
import de.piveau.metrics.linter.csv.utils.formatting.JsonResult;
import de.piveau.metrics.linter.csv.utils.formatting.LimitedRdfResult;
import de.piveau.metrics.linter.csv.utils.formatting.RdfResult;
import io.piveau.utils.JenaUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.RDFFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * This class contains the Result of a validation.
 *
 * @author Torben Jastrow
 * @version 1.0
 */
public class Result {


    /**
     * This represents a single validation result for one indicator., with the row and column the result is found in.
     *
     * @param indicator  The indicator that is checked.
     * @param row        The row the result is found in.
     * @param column     The column the result is found in.
     * @param columnName The name of the column the result is found in.
     */
    public record ResultItem(Indicator indicator, long row, Long column, String columnName) {

        /**
         * This method returns the type of the indicator, Error, Warning or Information.
         *
         * @return The type of the indicator.
         */
        public Indicator.IndicatorType getType() {
            return indicator.getType();
        }

        public String getMessage() {
            return indicator.getMessage();
        }

        public String getName() {
            return indicator.getName();
        }


        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ResultItem that = (ResultItem) o;

            if (row != that.row) return false;
            if (!indicator.equals(that.indicator)) return false;
            if (!Objects.equals(column, that.column)) return false;
            return Objects.equals(columnName, that.columnName);
        }

        @Override
        public int hashCode() {
            int result = indicator.hashCode();
            result = 31 * result + (int) (row ^ (row >>> 32));
            result = 31 * result + (int) (column ^ (column >>> 32));
            result = 31 * result + (columnName != null ? columnName.hashCode() : 0);
            return result;
        }
    }

    /**
     * This is the list of all results.
     *
     * @see ResultItem
     * @see Result
     */
    private final Set<ResultItem> items = new HashSet<>();

    //logger
    private static final Logger logger = LoggerFactory.getLogger(Result.class);


    private Integer limit = 0;

    /**
     * This is the set of all indicators that are checked.
     */
    private final Set<Indicator> toCheck;

    /**
     * The number of rows the CSV has.
     */
    private long rowCount = 0;

    /**
     * Creating a new Result with a specific set of indicators to check.
     *
     * @param toCheck The set of indicators that are checked.
     */
    public Result(Set<Indicator> toCheck) {
        this.toCheck = toCheck;
    }

    /**
     * Creating a new Result with all indicators to check.
     */
    public Result() {
        toCheck = new HashSet<>(Set.of(ErrorIndicator.values()));
        toCheck.addAll(Set.of(WarningIndicator.values()));
        toCheck.addAll(Set.of(WarningIndicator.values()));

    }

    /**
     * Setting the limit of indicators that should be checked
     *
     * @param limit
     */
    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    /**
     * Check if the item number is below the limit
     *
     * @return true if the limit is not reached
     */
    public boolean checkBelowLimit() {
        return limit == 0 || items.size() < limit;
    }


    /**
     * Adding a new result to the list of results, only if the indicator is in the set of indicators to check.
     *
     * @param indicator  The indicator that is checked.
     * @param row        The row the result is found in.
     * @param column     The column the result is found in.
     * @param columnName The name of the column the result is found in.
     * @return if the result item size is still under the limit.
     */
    public boolean addIndicator(Indicator indicator, Long row, Long column, String columnName) {

        if (limit != 0 && items.size() >= limit) {
            return false;
        }
        if (toCheck.contains(indicator)) {
            logger.debug("Adding indicator: " + indicator.getName() + " at row: " + row + " column: " + columnName + "(" + column + ")");
            items.add(new ResultItem(indicator, row, column, columnName));
        }
        return true;


    }

    /**
     * Tests if a specific indicator should be checked.
     *
     * @param indicator The indicator to test for.
     * @return if the indicator should be checked.
     */
    public boolean checkingIndicator(Indicator indicator) {
        return toCheck.contains(indicator);
    }

    /**
     * Test if the result contains any indicators of the type Error.
     *
     * @return if the result contains any indicators of the type Error.
     */
    public boolean isPassed() {
        return !hasErrors();
    }

    /**
     * Get all Result items that are in the result.
     *
     * @return all Result items that are in the result.
     */
    public Set<ResultItem> getItems() {
        return items;
    }



    /**
     * Check if the result contains any errors.
     *
     * @return true if the result contains any errors.
     */
    public Boolean hasErrors() {
        return contains(Indicator.IndicatorType.ERROR);
    }

    /**
     * Get the number of indicators of a specific {@link Indicator.IndicatorType} in the result.
     *
     * @param type The type of the indicator.
     * @return the number of indicators.
     */
    public long count(Indicator.IndicatorType type) {
        return items.stream().filter(it -> it.getType() == type).count();
    }

    /**
     * Get the number of indicators in the result.
     *
     * @return the number of indicators in the result.
     */
    public long count() {
        return items.size();
    }

    /**
     * Check if the result contains any warnings.
     *
     * @return true if the result contains any warnings.
     */
    public Boolean hasWarnings() {
        return contains(Indicator.IndicatorType.WARNING);
    }

    /**
     * Check if the result contains any information.
     *
     * @return true if the result contains any information.
     */
    public Boolean hasInfos() {
        return contains(Indicator.IndicatorType.INFORMATION);
    }

    /**
     * the number of rows the CSV has.
     *
     * @return the number of rows the CSV has.
     */
    public long getRowCount() {
        return rowCount;
    }

    /**
     * Set the number of rows the CSV has.
     *
     * @param rowCount the number of rows the CSV has.
     */
    protected void setRowCount(long rowCount) {
        this.rowCount = rowCount;
    }

    /**
     * Check if the result contains any {@link ResultItem} with a specific {@link Indicator}.
     *
     * @param indicator The indicator to check for.
     * @return true if the result contains this indicator at least once.
     */
    public boolean contains(Indicator indicator) {
        return items.stream().anyMatch(i -> i.indicator().equals(indicator));
    }

    /**
     * Check if the result contains any indicators of a specific {@link Indicator.IndicatorType}.
     *
     * @param type The type of the indicator.
     * @return true if the result contains any indicators of this type.
     */
    public boolean contains(Indicator.IndicatorType type) {
        return items.stream().anyMatch(i -> i.getType() == type);
    }

    /**
     * Get all {@link ResultItem}s with a specific {@link Indicator.IndicatorType}.
     *
     * @param type The type of the indicator.
     * @return all {@link ResultItem}s with this type.
     */
    public List<ResultItem> getIndicators(Indicator.IndicatorType type) {
        return items.stream().filter(i -> i.getType() == type).collect(Collectors.toList());
    }

    /**
     * Get all {@link ResultItem}s with a specific {@link Indicator}.
     *
     * @param indicator The indicator the {@link ResultItem}s have to contain.
     * @return all {@link ResultItem}s with this indicator.
     */
    public List<ResultItem> getIndicators(Indicator indicator) {
        return items.stream().filter(i -> i.getName().equals(indicator.getName())).collect(Collectors.toList());
    }

    /**
     * Return the result as JSON formatted string
     *
     * @return
     */
    public String toJson() {
        JsonResult jsonResult = new JsonResult(this);
        return jsonResult.getJson().encodePrettily();
    }

    /**
     * Return the result in an DQV graph as N3
     * @return
     */
    public String toNtriples() {
        return toRdf(RDFFormat.NTRIPLES);
    }

    /**
     * Return the result in an DQV graph as N3
     * @param resource the resource uri to use for the subject
     * @return
     */
    public String toNtriples(String resource) {
        return toRdf(RDFFormat.NTRIPLES, resource);
    }




    /**
     * Return the result in an DQV graph as jsonld
     * @return
     */
    public String toJsonLD() {
        return toRdf(RDFFormat.JSONLD);
    }

    /**
     * Return the result in an DQV graph as jsonld
     * @param resource the resource uri to use for the subject
     * @return
     */
    public String toJsonLD(String resource) {
        return toRdf(RDFFormat.JSONLD, resource);
    }

    /**
     * Return the result in an DQV graph in the specified RDFFormat
     * @param format the format to convert the model into
     * @return
     */
    public String toRdf(RDFFormat format) {
        RdfResult rdfResult = new RdfResult.Builder(this).build();
        return JenaUtils.write(rdfResult.getModel(), format);
    }

    /**
     * Return the result in an DQV graph in the specified RDFFormat
     *
     * @param format   the format to convert the model into
     * @param resource the resource uri to use for the subject
     * @return
     */
    public String toRdf(RDFFormat format, String resource) {
        RdfResult rdfResult = new RdfResult.Builder(this, resource).build();
        return JenaUtils.write(rdfResult.getModel(), format);
    }

    /**
     * Return the result in an DQV graph in the specified RDFFormat
     *
     * @param format the format to convert the model into
     * @param limit  the maximum number of rows to include in the result
     * @return
     */
    public String toRdf(RDFFormat format, Integer limit) {
        RdfResult rdfResult = new LimitedRdfResult.Builder(this, limit).build();
        return JenaUtils.write(rdfResult.getModel(), format);
    }

    /**
     * Return the result in an DQV graph in the specified RDFFormat
     *
     * @param format   the format to convert the model into
     * @param resource the resource uri to use for the subject
     * @param limit    the maximum number of rows to include in the result
     * @return
     */
    public String toRdf(RDFFormat format, String resource, Integer limit) {
        RdfResult rdfResult = new LimitedRdfResult.Builder(this, resource, limit).build();
        return JenaUtils.write(rdfResult.getModel(), format);
    }

    /**
     * Return the result in an DQV graph as RDF model
     *
     * @return
     */
    public Model toModel() {
        RdfResult rdfResult = new RdfResult.Builder(this).build();
        return rdfResult.getModel();
    }

    /**
     * Return the result in an DQV graph as RDF model
     *
     * @param resource the resource uri to use for the subject
     * @return
     */
    public Model toModel(String resource) {
        RdfResult rdfResult = new RdfResult.Builder(this, resource).build();
        return rdfResult.getModel();
    }

    /**
     * Return the result in an DQV graph as RDF model
     *
     * @param limit the maximum number of rows to include in the result
     * @return
     */
    public Model toModel(Integer limit) {
        RdfResult rdfResult = new LimitedRdfResult.Builder(this, limit).build();
        return rdfResult.getModel();
    }

    /**
     * Return the result in an DQV graph as RDF model
     *
     * @param resource the resource uri to use for the subject
     * @param limit    the maximum number of rows to include in the result
     * @return
     */
    public Model toModel(String resource, Integer limit) {
        RdfResult rdfResult = new LimitedRdfResult.Builder(this, resource, limit).build();
        return rdfResult.getModel();
    }
}
