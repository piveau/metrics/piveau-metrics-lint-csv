package de.piveau.metrics.linter.csv.indicators;


public interface Indicator {


    String getName();

    String getMessage();

    String getTitle();

    IndicatorType getType();


    enum IndicatorType {
        INFORMATION,
        WARNING,
        ERROR
    }


}
