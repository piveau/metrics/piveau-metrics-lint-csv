package de.piveau.metrics.linter.csv.utils.formatting;


import de.piveau.metrics.linter.csv.indicators.Indicator;
import de.piveau.metrics.linter.csv.validation.Result;
import io.piveau.vocabularies.vocabulary.DQV;
import io.piveau.vocabularies.vocabulary.PV;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.OA;
import org.apache.jena.vocabulary.RDFS;

public class LimitedRdfResult extends RdfResult {

    // Builder for returning the result


    private int count = 0;

    private int THRESHOLD;

    public static class Builder {

        private static final Integer DEFAULT_THRESHOLD = 2000;
        protected LimitedRdfResult rdfResult;
        private Result result;
        private String resourceUri = "urn:CSV";
        private Integer threshold = DEFAULT_THRESHOLD;

        public Builder(Result result, String resourceUri, Integer threshold) {
            this.result = result;
            this.resourceUri = resourceUri;
            if (threshold != null && threshold > 0) {
                this.threshold = threshold;
            }
        }

        public Builder(Result result, Integer threshold) {
            this.result = result;
            if (threshold != null && threshold > 0) {
                this.threshold = threshold;
            }

        }

        public Builder setThreshold(Integer threshold) {
            if (threshold != null && threshold > 0) {
                this.threshold = threshold;
            } else {
                this.threshold = DEFAULT_THRESHOLD;
            }
            return this;
        }

        public Builder(Result result) {
            this.result = result;

        }

        public Builder(Result result, String resourceUri) {
            this.result = result;
            this.resourceUri = resourceUri;

        }

        public Builder(LimitedRdfResult rdfResult) {
            this.rdfResult = rdfResult;
        }

        public Builder setResourceUri(String resourceUri) {
            this.resourceUri = resourceUri;
            return this;
        }

        public RdfResult build() {
            rdfResult = new LimitedRdfResult(result, resourceUri, threshold);
            return this.rdfResult.generate();
        }


    }

    protected LimitedRdfResult(Result result, Integer limit) {
        super(result);
        THRESHOLD = limit;
    }

    protected LimitedRdfResult(Result result, String resourceUri, Integer limit) {
        super(result, resourceUri);
        THRESHOLD = limit;
    }


    private RdfResult generate() {


        for (Result.ResultItem item : result.getIndicators(Indicator.IndicatorType.WARNING)) {
            if (count > THRESHOLD) break;
            addWarning(model, item);
            count++;

        }
        for (Result.ResultItem item : result.getIndicators(Indicator.IndicatorType.ERROR)) {
            if (count > THRESHOLD) break;
            addError(model, item);
            count++;
        }
        for (Result.ResultItem item : result.getIndicators(Indicator.IndicatorType.INFORMATION)) {
            if (count > THRESHOLD) break;
            addInformation(model, item);
            count++;
        }


        Resource body = model.createResource(PV.csvIndicator);
        body.addLiteral(PV.rowNumber, result.getRowCount());
        body.addLiteral(PV.passed, result.isPassed());

        if (count > THRESHOLD) {
            body.addLiteral(RDFS.comment, THRESHOLD);
        }


        Resource annotation = model.createResource(DQV.QualityAnnotation);

        annotation.addProperty(OA.motivatedBy, OA.assessing);
        annotation.addProperty(OA.hasBody, body);


        model.add(resource, DQV.hasQualityAnnotation, annotation);
        return this;
    }


}
