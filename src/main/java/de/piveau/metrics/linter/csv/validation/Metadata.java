package de.piveau.metrics.linter.csv.validation;



import de.piveau.metrics.linter.csv.indicators.ErrorIndicator;
import de.piveau.metrics.linter.csv.indicators.InformationIndicator;
import de.piveau.metrics.linter.csv.indicators.WarningIndicator;
import de.piveau.metrics.linter.csv.utils.SeparatorDetector;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * This class analyses a the file itself and the content type.
 * These are not checks regarding the CSV content
 * It also creates a {@link Result} object with the results of the analysis.
 */
public class Metadata {
    private final Result result;

    public Character getDetectedSeparator() {
        if(detectedSeparator!=null && detectedSeparator != ';'){
            result.addIndicator(WarningIndicator.DELIMITER,-1L,-1L,"");
        }

        return detectedSeparator;
    }

    private enum LineBreaks {
        LF, CRLF, CR, UNKNOWN
    }

    private final String fileName;
    private final String contentType;

    public String getCharset() {
        return charset;
    }

    private String charset = "UTF-8";

    private Character detectedSeparator = null;

    public Result getResult() {
        return result;
    }

    String getFileName() {
        return fileName;
    }

    String getContentType() {
        return contentType;
    }

    /**
     * The default metadata constructor
     * @param result    The result object.
     * @param fileName The file name.
     * @param contentType The content type of the file.
     */
    Metadata(Result result, String fileName, String contentType) {
        Objects.requireNonNull(result, "result must not be null");
        Objects.requireNonNull(fileName, "fileName must not be null");
        this.result = result;
        this.fileName = fileName;
        this.contentType = contentType;
    }


    /**
     * This file checks the content type of the file.
     * The result is stored in the {@link Result} object.
     * If a charset is found, it is stored in the {@link Metadata} object.
     */
    public Metadata checkContentType() {

        if (contentType == null || contentType.isEmpty()) {
            result.addIndicator(WarningIndicator.NO_CONTENT_TYPE, -1L, -1L,"");
            if (fileName.endsWith(".xls") || fileName.endsWith(".xlsx")) {
                result.addIndicator(WarningIndicator.EXCEL, -1L, -1L,"");
            }
            return this;
        }
        String[] cts = contentType.split(";");
        if (cts.length == 0) {
            result.addIndicator(WarningIndicator.NO_CONTENT_TYPE, -1L, -1L,"");
            return this;
        } else if (!contentType.contains("text/csv")) {
            result.addIndicator(ErrorIndicator.WRONG_CONTENT_TYPE, -1L,-1L, "");

        }

        if (cts.length > 1 && cts[1].contains("charset")) {
            String charset = cts[1].split("=")[1];
            if (!charset.equalsIgnoreCase("UTF-8")) {
                result.addIndicator(WarningIndicator.ENCODING, -1L,-1L, "");
                this.charset = charset.toUpperCase();
            }
        } else {
            result.addIndicator(WarningIndicator.NO_ENCODING, -1L, -1L,"");
        }
        return this;

    }

    /**
     * This file checks the content of the file for invalid characters.
     * Errors will be generated for non-standard line breaks and encoding errors.
     * If {@link #checkContentType()} has been called and a charset is found, the charset is used to check the content. Otherwise the default charset is used.
     *
     * @throws IOException if an I/O error occurs
     */
    public Metadata checkFile() throws IOException {
        return checkFile(charset);
    }

    /**
     * This file checks the content of the file for invalid characters.
     * Errors will be generated for non-standard line breaks and encoding errors.
     *
     * @throws IOException if an I/O error occurs
     */
    public Metadata checkFile(String charset) throws IOException {

        Objects.requireNonNull(charset, "charset must not be null");

        List<LineBreaks> linebreaks = new ArrayList<>();
        Charset cs = Charset.forName(charset);
        CharsetDecoder decoder = cs.newDecoder().onMalformedInput(CodingErrorAction.REPORT);
        decoder.onUnmappableCharacter(CodingErrorAction.REPORT);

        SeparatorDetector separatorDetector = new SeparatorDetector();

        Path path = Path.of(fileName);

        Pattern pat = Pattern.compile(".*\\R|.+\\z");
        try (Scanner in = new Scanner(path, cs)) {
            String line;
            Long lineNumber = 0L;


            while ((line = in.findWithinHorizon(pat, 0)) != null) {


                if (line.endsWith("\r\n")) {
                    //System.out.println("CRLF");

                    linebreaks.add(LineBreaks.CRLF);

                } else if (line.endsWith("\r")) {
                    //System.out.println("CR");
                    linebreaks.add(LineBreaks.CR);


                    result.addIndicator(InformationIndicator.NONRFC_LINE_BREAKS, lineNumber, -1L,"");
                } else if (line.endsWith("\n")) {
                    //System.out.println("line break: LF");
                    linebreaks.add(LineBreaks.LF);
                    result.addIndicator(InformationIndicator.NONRFC_LINE_BREAKS, lineNumber, -1L,"");

                } else if (!line.matches(".*\\z")) {
                    linebreaks.add(LineBreaks.UNKNOWN);
                    result.addIndicator(InformationIndicator.NONRFC_LINE_BREAKS, lineNumber, -1L,"");
                    //System.out.println("line break: unknown");

                }
                separatorDetector.detect(line);

                try {
                    decoder.decode(ByteBuffer.wrap(line.getBytes(cs)));



                } catch (MalformedInputException e) {
                    result.addIndicator(ErrorIndicator.INVALID_ENCODING, lineNumber, -1L,"");
                    System.out.println("MalformedInputException");
                } catch (CharacterCodingException ex) {
                    result.addIndicator(ErrorIndicator.INVALID_ENCODING, lineNumber,-1L, "");
                    System.out.println("CharacterCodingException");
                    throw new RuntimeException(ex);
                }
                lineNumber++;

            }
        }
        detectedSeparator = separatorDetector.getMostFrequentSeparator();


        //check if line breaks are consistent

            if (linebreaks.stream().distinct().count() >1) {
                //System.out.println("linebreaks: " + linebreaks);
                result.addIndicator(ErrorIndicator.LINE_BREAKS, -1L,-1L, "");
            }

        return this;
    }


}
