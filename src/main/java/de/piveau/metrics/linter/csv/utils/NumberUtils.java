package de.piveau.metrics.linter.csv.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class NumberUtils {

    public enum NumberType {
        INTEGER,
        DECIMAL,
        UNKNOWN
    }

    private static final Pattern dqgRecommendation1 = Pattern.compile("^[+-]{0,1}\\d+$");
    private static final Pattern dqgRecommendation2 = Pattern.compile("^[+-]{0,1}\\d+\\.\\d+$");

    /**
     * Contains Regular Expressions to match numbers that are either integers or decimals.
     *  The number 1234567,89 is used as an example for the different matches and is modified to match the different expressions.
     */
    private static final Map<Pattern, NumberType> NUMBER_FORMAT_REGEXPS = new HashMap<>() {{
        put(Pattern.compile("^[+-]{0,1}\\d+$"), NumberType.INTEGER); // 1234567
        put(Pattern.compile("^[+-]{0,1}\\d{1,3}(?:\\s\\d{3})+$"), NumberType.INTEGER); // 1 234 567 or 234 567 or 34 567
        put(Pattern.compile("^[+-]{0,1}\\d{1,3}(?:[\\.]\\d{3}){2,}$"), NumberType.INTEGER); //  1.234.567 but not 234.567 (234,567 can be integer or decimal, only decimal is correct so it will be recognised as decimal. It should not create an indicator either way)
        put(Pattern.compile("^[+-]{0,1}\\d{1,3}(?:[\\,]\\d{3})+$"), NumberType.INTEGER); // 1,234,567  or 234,567 (234,567 can be integer or decimal, but both is incorrect according to the DQG)
        put(Pattern.compile("^[+-]{0,1}\\d+\\.\\d+$"), NumberType.DECIMAL); //0.89 or 1234567.89
        put(Pattern.compile("^[+-]{0,1}0,\\d+$"), NumberType.DECIMAL); //0,89
        put(Pattern.compile("^[+-]{0,1}\\d{1,3}(?:\\s\\d{3})+,\\d+$"), NumberType.DECIMAL); // 1 234 567,89
        put(Pattern.compile("^[+-]{0,1}\\d{1,3}(?:\\s\\d{3})*\\.\\d+$"), NumberType.DECIMAL); // 1 234 567.89
        put(Pattern.compile("^[+-]{0,1}\\d{1,3}(?:\\.\\d{3})+,\\d+$"), NumberType.DECIMAL); // 1.234.567,89 or 234.567,89
        put(Pattern.compile("^[+-]{0,1}\\d{1,3}(?:,\\d{3})+\\.\\d+$"), NumberType.DECIMAL); // 1,234,567.89 or 234,567.89
    }};

    public static NumberType getNumberType(String value) {
        for (Map.Entry<Pattern, NumberType> entry : NUMBER_FORMAT_REGEXPS.entrySet()) {
            if (entry.getKey().matcher(value).matches()) {
                return entry.getValue();
            }
        }
        return NumberType.UNKNOWN;
    }


    /**
     * Checks if the given string is a number following the DQG-rules.
     * According to `1.1.3.2. Formatting of decimal numbers and numbers in the thousands`,
     * a correct number consists of either only integers or additionally a decimal point.
     *
     *
     * @param s the string to check
     * @return true if the string is following the recommendet format, false otherwise
     */
    public static boolean followsDQGRecommendation(String s) {
        return dqgRecommendation1.matcher(s).matches() || dqgRecommendation2.matcher(s).matches();
    }

}
