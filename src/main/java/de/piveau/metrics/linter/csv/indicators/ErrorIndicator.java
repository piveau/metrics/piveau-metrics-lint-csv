package de.piveau.metrics.linter.csv.indicators;




public enum ErrorIndicator implements Indicator {
    UNKNOWN_ERROR("unknown_error", "Unknown error", "Unknown Error"),
    DATE_FORMAT("date_format", "Date formats do not conform to a standard", "Date Format"),
    NUMBER_FORMAT("number_format", "Number formats do not conform to a standard", "Number Format"),
    NULL_VALUES("null_values", "Mark null values explicitly as such", "Null Values"),
    WRONG_CONTENT_TYPE("wrong_content_type", "Content type is not text/csv", "Wrong Content Type"),
    RAGGED_ROWS("ragged_rows", "Row has a different number of columns (than the first row in the file)", "Ragged Rows"),
    BLANK_ROWS("blank_rows", "Completely empty row, e.g. blank line or a line where all column values are empty", "Blank Rows"),
    INVALID_ENCODING("invalid_encoding", "Encoding error when parsing row, e.g. because of invalid characters", "Invalid Encoding"),
    NOT_FOUND("not_found", "HTTP 404 error when retrieving the data", "Not Found"),
    STRAY_QUOTE("stray_quote", "Missing or stray quote", "Stray Quote"),
    //UNCLOSED_QUOTE("unclosed_quote","unclosed quoted field"),
    WHITESPACE("whitespace", "A quoted column has leading or trailing whitespace", "Whitespace"),
    LINE_BREAKS("line_breaks", "Line breaks were inconsistent or incorrectly specified", "Line Breaks");


    private final String name;
    private final String message;

    private final String titel;


    ErrorIndicator(String name, String message, String titel) {
        this.name = name;
        this.message = message;
        this.titel = titel;

    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }

    public String getTitle() {
        return titel;
    }

    public IndicatorType getType() {
        return IndicatorType.ERROR;
    }


}
