package de.piveau.metrics.linter.csv.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class DateUtils {

    private static final Pattern ISO_DATE_FORMAT_REGEXP = Pattern.compile(
            "^([+-]?\\d{4}(?!\\d{2}\\b))((-?)((0[1-9]|1[0-2])(\\3([12]\\d|0[1-9]|3[01]))?|W([0-4]\\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\\d|[12]\\d{2}|3([0-5]\\d|6[1-6])))([T\\s]((([01]\\d|2[0-3])((:?)[0-5]\\d)?|24\\:?00)([\\.,]\\d+(?!:))?)?(\\17[0-5]\\d([\\.,]\\d+)?)?([zZ]|([\\+-])([01]\\d|2[0-3]):?([0-5]\\d)?)?)?)?$"
    );

    private static final Map<Pattern, String> DATE_FORMAT_REGEXPS = new HashMap<>() {{
        put(Pattern.compile("^\\d{8}$"), "yyyyMMdd");
        put(Pattern.compile("^\\d{1,2}-\\d{1,2}-\\d{4}$"), "d-M-yyyy");
        put(Pattern.compile("^\\d{4}-\\d{1,2}-\\d{1,2}$"), "yyyy-M-d");
        put(Pattern.compile("^\\d{1,2}/\\d{1,2}/\\d{4}$"), "M/d/yyyy");
        put(Pattern.compile("^\\d{1,2}\\.\\d{1,2}\\.\\d{4}$"), "d.M.yyyy");
        put(Pattern.compile("^\\d{4}/\\d{1,2}/\\d{1,2}$"), "yyyy/M/d");
        put(Pattern.compile("^\\d{1,2}\\s[a-zA-Z]{3}\\s\\d{4}$"), "dd MMM yyyy");
        put(Pattern.compile("^\\d{1,2}\\s[a-zA-Z]{4,}\\s\\d{4}$"), "dd MMMM yyyy");
        put(Pattern.compile("^\\d{4}-\\d{1,2}$"), "yyyy-M");
        // put("^$", "yyyy");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}$"), "yyyy-'W'w");
        put(Pattern.compile("^\\d{4}W\\d{1,2}$"), "yyyy'W'w");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}-\\d{1}$"), "yyyy-'W'w-e");
        put(Pattern.compile("^\\d{4}W\\d{1,2}\\d{1}$"), "yyyy'W'we");
        put(Pattern.compile("^\\d{4}-\\d{1,3}$"), "yyyy-D");
        // put("^\d{4}\d{1,3}$", "yyyyDDD");


        //put("^\\d{12}$", "yyyyMMddHHmm");
        put(Pattern.compile("^\\d{8}\\s\\d{4}$"), "yyyyMMdd Hm");
        put(Pattern.compile("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}$"), "d-M-yyyy H:m");
        put(Pattern.compile("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}$"), "yyyy-M-d H:m");
        put(Pattern.compile("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}$"), "M/d/yyyy H:m");
        put(Pattern.compile("^\\d{1,2}\\.\\d{1,2}\\.\\d{4}\\s\\d{1,2}:\\d{2}$"), "d.M.yyyy H:m");
        put(Pattern.compile("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}$"), "yyyy/M/d H:m");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}$"), "dd MMM yyyy H:m");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}$"), "dd MMMM yyyy H:m");
        put(Pattern.compile("^\\d{4}-\\d{1,2}\\s\\d{1,2}:\\d{2}$"), "yyyy-M H:m");
        put(Pattern.compile("^\\d{4}\\s\\d{1,2}:\\d{2}$"), "yyyy H:m");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}\\s\\d{1,2}:\\d{2}$"), "yyyy-'W'w H:m");
        put(Pattern.compile("^\\d{4}W\\d{1,2}\\s\\d{1,2}:\\d{2}$"), "yyyy'W'w H:m");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}-\\d{1}\\s\\d{1,2}:\\d{2}$"), "yyyy-'W'w-e H:m");
        put(Pattern.compile("^\\d{4}W\\d{1,2}\\d{1}\\s\\d{1,2}:\\d{2}$"), "yyyy'W'we H:m");
        put(Pattern.compile("^\\d{4}-\\d{1,3}\\s\\d{1,2}:\\d{2}$"), "yyyy-D H:m");
        put(Pattern.compile("^\\d{4}\\d{1,3}\\s\\d{1,2}:\\d{2}$"), "yyyyD H:m");


        // put("^\\d{14}$", "yyyyMMddHHmmss");
        put(Pattern.compile("^\\d{8}\\s\\d{6}$"), "yyyyMMdd HHmmss");
        put(Pattern.compile("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$"), "d-M-yyyy H:m:s");
        put(Pattern.compile("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy-M-d H:m:s");
        put(Pattern.compile("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$"), "M/d/yyyy H:m:s");
        put(Pattern.compile("^\\d{1,2}\\.\\d{1,2}\\.\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$"), "d.M.yyyy H:m:s");
        put(Pattern.compile("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy/M/d H:m:s");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$"), "dd MMM yyyy H:m:s");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$"), "dd MMMM yyyy H:m:s");
        put(Pattern.compile("^\\d{4}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy-MM H:m");
        put(Pattern.compile("^\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy H:m");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy-'W'w H:m");
        put(Pattern.compile("^\\d{4}W\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy'W'w H:m");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}-\\d{1}\\s\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy-'W'w-e H:m");
        put(Pattern.compile("^\\d{4}W\\d{1,2}\\d{1}\\s\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy'W'we H:m");
        put(Pattern.compile("^\\d{4}-\\d{1,3}\\s\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy-D H:m");
        // put("^\d{4}\d{1,3}\s\d{1,2}:\d{2}:\d{2}$", "yyyyDDD HH:mm");


        ///###########
        //with Timezone
        put(Pattern.compile("^\\d{8}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyyMMddXXX");
        put(Pattern.compile("^\\d{1,2}-\\d{1,2}-\\d{4}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "d-M-yyyyXXX");
        put(Pattern.compile("^\\d{4}-\\d{1,2}-\\d{1,2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-M-dXXX");
        put(Pattern.compile("^\\d{1,2}/\\d{1,2}/\\d{4}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "M/d/yyyyXXX");
        put(Pattern.compile("^\\d{1,2}\\.\\d{1,2}\\.\\d{4}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "d.M.yyyyXXX");
        put(Pattern.compile("^\\d{4}/\\d{1,2}/\\d{1,2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy/M/dXXX");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "dd MMM yyyyXXX");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "dd MMMM yyyyXXX");
        put(Pattern.compile("^\\d{4}-\\d{1,2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-MXXX");
        // put("^$", "yyyy");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-'W'wXXX");
        put(Pattern.compile("^\\d{4}W\\d{1,2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy'W'wXXX");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}-\\d{1}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-'W'w-eXXX");
        put(Pattern.compile("^\\d{4}W\\d{1,2}\\d{1}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy'W'weXXX");
        put(Pattern.compile("^\\d{4}-\\d{1,3}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-DXXX");
        // put("^\d{4}\d{1,3}$", "yyyyDDD");


        put(Pattern.compile("^\\d{12}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyyMMddHHmmXXX");
        put(Pattern.compile("^\\d{8}\\s\\d{4}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyyMMdd HmXXX");
        put(Pattern.compile("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "d-M-yyyy H:mXXX");
        put(Pattern.compile("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-M-d H:mXXX");
        put(Pattern.compile("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "M/d/yyyy H:mXXX");
        put(Pattern.compile("^\\d{1,2}\\.\\d{1,2}\\.\\d{4}\\s\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "d.M.yyyy H:mXXX");
        put(Pattern.compile("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy/M/d H:mXXX");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "dd MMM yyyy H:mXXX");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "dd MMMM yyyy H:mXXX");
        put(Pattern.compile("^\\d{4}-\\d{1,2}\\s\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-M H:mXXX");
        put(Pattern.compile("^\\d{4}\\s\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy H:mXXX");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}\\s\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-'W'w H:mXXX");
        put(Pattern.compile("^\\d{4}W\\d{1,2}\\s\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy'W'w H:mXXX");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}-\\d{1}\\s\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-'W'w-e H:mXXX");
        put(Pattern.compile("^\\d{4}W\\d{1,2}\\d{1}\\s\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy'W'we H:mXXX");
        put(Pattern.compile("^\\d{4}-\\d{1,3}\\s\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-D H:mXXX");
        put(Pattern.compile("^\\d{4}\\d{1,3}\\s\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyyD H:mXXX");


        ///put("^\\d{14}$", "yyyyMMddHHmmss");
        put(Pattern.compile("^\\d{8}\\s\\d{6}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyyMMdd HHmmssXXX");
        put(Pattern.compile("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "d-M-yyyy H:m:sXXX");
        put(Pattern.compile("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-M-d H:m:sXXX");
        put(Pattern.compile("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "M/d/yyyy H:m:sXXX");
        put(Pattern.compile("^\\d{1,2}\\.\\d{1,2}\\.\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "d.M.yyyy H:m:sXXX");
        put(Pattern.compile("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy/M/d H:m:sXXX");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "dd MMM yyyy H:m:sXXX");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "dd MMMM yyyy H:m:sXXX");
        put(Pattern.compile("^\\d{4}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-MM H:mXXX");
        put(Pattern.compile("^\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy H:mXXX");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-'W'w H:mXXX");
        put(Pattern.compile("^\\d{4}W\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy'W'w H:mXXX");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}-\\d{1}\\s\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-'W'w-e H:mXXX");
        put(Pattern.compile("^\\d{4}W\\d{1,2}\\d{1}\\s\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy'W'we H:mXXX");
        put(Pattern.compile("^\\d{4}-\\d{1,3}\\s\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-D H:mXXX");


        //###########
        // with T divider
        //#############


        put(Pattern.compile("^\\d{8}T$"), "yyyyMMdd'T'");
        put(Pattern.compile("^\\d{1,2}-\\d{1,2}-\\d{4}T$"), "d-M-yyyy'T'");
        put(Pattern.compile("^\\d{4}-\\d{1,2}-\\d{1,2}T$"), "yyyy-M-d'T'");
        put(Pattern.compile("^\\d{1,2}/\\d{1,2}/\\d{4}T$"), "M/d/yyyy'T'");
        put(Pattern.compile("^\\d{1,2}\\.\\d{1,2}\\.\\d{4}T$"), "d.M.yyyy'T'");
        put(Pattern.compile("^\\d{4}/\\d{1,2}/\\d{1,2}T$"), "yyyy/M/d'T'");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}T$"), "dd MMM yyyy'T'");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}T$"), "dd MMMM yyyy'T'");
        put(Pattern.compile("^\\d{4}-\\d{1,2}T$"), "yyyy-M'T'");
        // put("^$", "yyyy");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}T$"), "yyyy-'W'w'T'");
        put(Pattern.compile("^\\d{4}W\\d{1,2}T$"), "yyyy'W'w'T'");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}-\\d{1}T$"), "yyyy-'W'w-e'T'");
        put(Pattern.compile("^\\d{4}W\\d{1,2}\\d{1}T$"), "yyyy'W'we'T'");
        put(Pattern.compile("^\\d{4}-\\d{1,3}T$"), "yyyy-D'T'");
        // put("^\d{4}\d{1,3}$", "yyyyDDD");


        //put("^\\d{12}$", "yyyyMMddHHmm");
        put(Pattern.compile("^\\d{8}T\\d{4}$"), "yyyyMMdd'T'Hm");
        put(Pattern.compile("^\\d{1,2}-\\d{1,2}-\\d{4}T\\d{1,2}:\\d{2}$"), "d-M-yyyy'T'H:m");
        put(Pattern.compile("^\\d{4}-\\d{1,2}-\\d{1,2}T\\d{1,2}:\\d{2}$"), "yyyy-M-d'T'H:m");
        put(Pattern.compile("^\\d{1,2}/\\d{1,2}/\\d{4}T\\d{1,2}:\\d{2}$"), "M/d/yyyy'T'H:m");
        put(Pattern.compile("^\\d{1,2}\\.\\d{1,2}\\.\\d{4}T\\d{1,2}:\\d{2}$"), "d.M.yyyy'T'H:m");
        put(Pattern.compile("^\\d{4}/\\d{1,2}/\\d{1,2}T\\d{1,2}:\\d{2}$"), "yyyy/M/d'T'H:m");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}T\\d{1,2}:\\d{2}$"), "dd MMM yyyy'T'H:m");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}T\\d{1,2}:\\d{2}$"), "dd MMMM yyyy'T'H:m");
        put(Pattern.compile("^\\d{4}-\\d{1,2}T\\d{1,2}:\\d{2}$"), "yyyy-M'T'H:m");
        put(Pattern.compile("^\\d{4}T\\d{1,2}:\\d{2}$"), "yyyy'T'H:m");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}T\\d{1,2}:\\d{2}$"), "yyyy-'W'w'T'H:m");
        put(Pattern.compile("^\\d{4}W\\d{1,2}T\\d{1,2}:\\d{2}$"), "yyyy'W'w'T'H:m");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}-\\d{1}T\\d{1,2}:\\d{2}$"), "yyyy-'W'w-e'T'H:m");
        put(Pattern.compile("^\\d{4}W\\d{1,2}\\d{1}T\\d{1,2}:\\d{2}$"), "yyyy'W'we'T'H:m");
        put(Pattern.compile("^\\d{4}-\\d{1,3}T\\d{1,2}:\\d{2}$"), "yyyy-D'T'H:m");
        put(Pattern.compile("^\\d{4}\\d{1,3}T\\d{1,2}:\\d{2}$"), "yyyyD'T'H:m");


        // put("^\\d{14}$", "yyyyMMddHHmmss");
        put(Pattern.compile("^\\d{8}T\\d{6}$"), "yyyyMMdd'T'HHmmss");
        put(Pattern.compile("^\\d{1,2}-\\d{1,2}-\\d{4}T\\d{1,2}:\\d{2}:\\d{2}$"), "d-M-yyyy'T'H:m:s");
        put(Pattern.compile("^\\d{4}-\\d{1,2}-\\d{1,2}T\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy-M-d'T'H:m:s");
        put(Pattern.compile("^\\d{1,2}/\\d{1,2}/\\d{4}T\\d{1,2}:\\d{2}:\\d{2}$"), "M/d/yyyy'T'H:m:s");
        put(Pattern.compile("^\\d{1,2}\\.\\d{1,2}\\.\\d{4}T\\d{1,2}:\\d{2}:\\d{2}$"), "d.M.yyyy'T'H:m:s");
        put(Pattern.compile("^\\d{4}/\\d{1,2}/\\d{1,2}T\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy/M/d'T'H:m:s");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}T\\d{1,2}:\\d{2}:\\d{2}$"), "dd MMM yyyy'T'H:m:s");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}T\\d{1,2}:\\d{2}:\\d{2}$"), "dd MMMM yyyy'T'H:m:s");
        put(Pattern.compile("^\\d{4}-\\d{1,2}T\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy-MM'T'H:m");
        put(Pattern.compile("^\\d{4}T\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy'T'H:m");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}T\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy-'W'w'T'H:m");
        put(Pattern.compile("^\\d{4}W\\d{1,2}T\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy'W'w'T'H:m");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}-\\d{1}T\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy-'W'w-e'T'H:m");
        put(Pattern.compile("^\\d{4}W\\d{1,2}\\d{1}T\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy'W'we'T'H:m");
        put(Pattern.compile("^\\d{4}-\\d{1,3}T\\d{1,2}:\\d{2}:\\d{2}$"), "yyyy-D'T'H:m");
        // put("^\d{4}\d{1,3}\s\d{1,2}:\d{2}:\d{2}$", "yyyyDDD HH:mm");


        ///###########
        //with Timezone
        put(Pattern.compile("^\\d{8}T([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyyMMdd'T'XXX");
        put(Pattern.compile("^\\d{1,2}-\\d{1,2}-\\d{4}T([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "d-M-yyyy'T'XXX");
        put(Pattern.compile("^\\d{4}-\\d{1,2}-\\d{1,2}T([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-M-d'T'XXX");
        put(Pattern.compile("^\\d{1,2}/\\d{1,2}/\\d{4}T([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "M/d/yyyy'T'XXX");
        put(Pattern.compile("^\\d{1,2}\\.\\d{1,2}\\.\\d{4}T([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "d.M.yyyy'T'XXX");
        put(Pattern.compile("^\\d{4}/\\d{1,2}/\\d{1,2}T([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy/M/d'T'XXX");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}T([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "dd MMM yyyy'T'XXX");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}T([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "dd MMMM yyyy'T'XXX");
        put(Pattern.compile("^\\d{4}-\\d{1,2}T([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-M'T'XXX");
        // put("^$", "yyyy");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}T([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-'W'w'T'XXX");
        put(Pattern.compile("^\\d{4}W\\d{1,2}T([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy'W'w'T'XXX");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}-\\d{1}T([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-'W'w-e'T'XXX");
        put(Pattern.compile("^\\d{4}W\\d{1,2}\\d{1}T([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy'W'we'T'XXX");
        put(Pattern.compile("^\\d{4}-\\d{1,3}T([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-D'T'XXX");
        // put("^\d{4}\d{1,3}$", "yyyyDDD");


        put(Pattern.compile("^\\d{8}T\\d{4}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyyMMdd'T'HmXXX");
        put(Pattern.compile("^\\d{1,2}-\\d{1,2}-\\d{4}T\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "d-M-yyyy'T'H:mXXX");
        put(Pattern.compile("^\\d{4}-\\d{1,2}-\\d{1,2}T\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-M-d'T'H:mXXX");
        put(Pattern.compile("^\\d{1,2}/\\d{1,2}/\\d{4}T\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "M/d/yyyy'T'H:mXXX");
        put(Pattern.compile("^\\d{1,2}\\.\\d{1,2}\\.\\d{4}T\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "d.M.yyyy'T'H:mXXX");
        put(Pattern.compile("^\\d{4}/\\d{1,2}/\\d{1,2}T\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy/M/d'T'H:mXXX");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}T\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "dd MMM yyyy'T'H:mXXX");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}T\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "dd MMMM yyyy'T'H:mXXX");
        put(Pattern.compile("^\\d{4}-\\d{1,2}T\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-M'T'H:mXXX");
        put(Pattern.compile("^\\d{4}T\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy'T'H:mXXX");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}T\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-'W'w'T'H:mXXX");
        put(Pattern.compile("^\\d{4}W\\d{1,2}T\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy'W'w'T'H:mXXX");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}-\\d{1}T\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-'W'w-e H:mXXX");
        put(Pattern.compile("^\\d{4}W\\d{1,2}\\d{1}T\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy'W'we'T'H:mXXX");
        put(Pattern.compile("^\\d{4}-\\d{1,3}T\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-D'T'H:mXXX");
        put(Pattern.compile("^\\d{4}\\d{1,3}T\\d{1,2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyyD'T'H:mXXX");


        ///put("^\\d{14}$", "yyyyMMddHHmmss");
        put(Pattern.compile("^\\d{8}T\\d{6}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyyMMdd'T'HHmmssXXX");
        put(Pattern.compile("^\\d{1,2}-\\d{1,2}-\\d{4}T\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "d-M-yyyy'T'H:m:sXXX");
        put(Pattern.compile("^\\d{4}-\\d{1,2}-\\d{1,2}T\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-M-d'T'H:m:sXXX");
        put(Pattern.compile("^\\d{1,2}/\\d{1,2}/\\d{4}T\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "M/d/yyyy'T'H:m:sXXX");
        put(Pattern.compile("^\\d{1,2}\\.\\d{1,2}\\.\\d{4}T\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "d.M.yyyy'T'H:m:sXXX");
        put(Pattern.compile("^\\d{4}/\\d{1,2}/\\d{1,2}T\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy/M/d'T'H:m:sXXX");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}T\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "dd MMM yyyy'T'H:m:sXXX");
        put(Pattern.compile("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}T\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "dd MMMM yyyy'T'H:m:sXXX");
        put(Pattern.compile("^\\d{4}-\\d{1,2}T\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-MM'T'H:mXXX");
        put(Pattern.compile("^\\d{4}T\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy'T'H:mXXX");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}T\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-'W'w'T'H:mXXX");
        put(Pattern.compile("^\\d{4}W\\d{1,2}T\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy'W'w'T'H:mXXX");
        put(Pattern.compile("^\\d{4}-W\\d{1,2}-\\d{1}T\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-'W'w-e'T'H:mXXX");
        put(Pattern.compile("^\\d{4}W\\d{1,2}\\d{1}T\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy'W'we'T'H:mXXX");
        put(Pattern.compile("^\\d{4}-\\d{1,3}T\\d{1,2}:\\d{2}:\\d{2}([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?)$"), "yyyy-D'T'H:mXXX");


    }};


    /**
     * Check if this can be a date format
     *
     * @param date String date to be parsed
     * @return the format to parse this as a date
     */
    public static String getDateFormat(String date) {
        for (Pattern pattern : DATE_FORMAT_REGEXPS.keySet()) {
            if (pattern.matcher(date).matches()) {
                return DATE_FORMAT_REGEXPS.get(pattern);
            }
        }
        return null;
    }

    /**
     * Check if this matches the datetime format pattern
     *
     * @param date String date to be parsed
     * @return the format to parse this as a date
     */
    public static boolean isDateTime(String date) {
        Pattern pattern = Pattern.compile("^(?:\\d{4}(?:-\\d{1,3}|-?W\\d{1,3}(?:-[1234567])?|[-/][012]?\\d[-/][0123]?\\d)[T\\s]?|[012]?\\d[-/.][0123]?\\d[-/.](?:\\d{4}|\\d{2})|[012]?\\d\\.?\\s\\w+\\.?\\s(?:\\d{4}|\\d{2}))(?:\\d{1,2}:\\d{2}:\\d{2}(\\.\\d{3})?(([zZ]|([+-])([01]\\d|2[0-3]):?([0-5]\\d)?))?)?$");
        return pattern.matcher(date).matches();
    }

    /**
     * Check if the date  is in ISO format
     *
     * @param date String the date to check
     * @return true if the date is in ISO format
     */
    public static Boolean isISODateFormat(String date) {
        return ISO_DATE_FORMAT_REGEXP.matcher(date).matches();
    }

}
