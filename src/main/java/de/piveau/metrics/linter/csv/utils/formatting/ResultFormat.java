package de.piveau.metrics.linter.csv.utils.formatting;

import de.piveau.metrics.linter.csv.validation.Result;

public interface ResultFormat {

    // a builder for building and generating the ResultFormat
    abstract class Builder {

        protected Result result;
        protected String resourceUri;

        public Builder(Result result) {
            this.result = result;
        }

        public Builder setResourceUri(String resourceUri) {
            this.resourceUri = resourceUri;
            return this;
        }

        public abstract ResultFormat build();
    }


}
