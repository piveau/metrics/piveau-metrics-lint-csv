package de.piveau.metrics.linter.csv.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class NumberUtilsTest {

    @Test
    @DisplayName("Testing integers")
    void testInteger() {
        assertEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("1"), "1 is an integer");
        assertEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("-1"), "-1 is an integer");
        assertEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("0"), "0 is an integer");
        assertEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("123456789"), "123456789 is an integer");
        assertEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("-123456789"), "-123456789 is an integer");
        assertEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("123 456 789"), " 123 456 789 is an integer");
        assertEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("-123 456 789"), "-123 456 789 is an integer");
        assertEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("234,567"), "234,567 is an integer"); // 234,567 can be integer or decimal, but both is not according to the DQG. Thus we identify it as integer and show an error for it
        assertEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("-234,567,890"), "-234,567,890 is an integer");
        assertEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("234.567.890"), "234.567.890 is an integer"); // Having the same seperator twice means that this is an integer


        assertNotEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("234.567"), "234.567 is not an integer"); // 234.567 can be integer or decimal, only decimal would be correct according to the DQG. Thus we identify it as decimal and show no error for it to avoid false positives
        assertNotEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("789.654.8"), "789.654.8 is not an integer");

        assertNotEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("1.0"), "1.0 is not an integer");
        assertNotEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("123 456 789.0"), "123 456 789.0 is not an integer");
        assertNotEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("123 456 789.00"), "123 456 789.00 is not an integer");

        assertNotEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("1.0.0"), "1.0.0 is not an integer");
        assertNotEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("String"), "String is not an integer");
        assertNotEquals(NumberUtils.NumberType.INTEGER, NumberUtils.getNumberType("2020-20-11"), "2020-20-11 is not an integer");

    }

    @Test
    @DisplayName("Testing decimals")
    void testDecimal() {
        assertEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("1.0"), "1.0 is a decimal");
        assertEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("-1.0"), "-1.0 is a decimal");
        assertEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("0.0"), "0.0 is a decimal");
        assertEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("123456789.0"), "123456789.0 is a decimal");
        assertEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("-123456789.0"), "-123456789.0 is a decimal");
        assertEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("123 456 789.0"), " 123 456 789.0 is a decimal");
        assertEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("-123 456 789.0"), "-123 456 789.0 is a decimal");
        assertEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("234,567.0"), "234,567.0 is a decimal");
        assertEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("234,567.00"), "234,567.00 is a decimal");
        assertEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("234,567.000"), "234,567.000 is a decimal");
        assertEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("234.567"), "234.567 is a decimal"); // 234.567 can be integer or decimal, only decimal would be correct according to the DQG. Thus we identify it as decimal and show no error for it to avoid false positives


        assertNotEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("1"), "1 is not a decimal");
        assertNotEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("-1"), "-1 is not a decimal");
        assertNotEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("0"), "0 is not a decimal");
        assertNotEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("123456789"), "123456789 is not a decimal");
        assertNotEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("-123456789"), "-123456789 is not a decimal");
        assertNotEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("123 456 789"), " 123 456 789 is not a decimal");
        assertNotEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("-123 456 789"), "-123 456 789 is not a decimal");
        assertNotEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("234.567.890"), "234.567.890 is not a decimal");
        assertNotEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("234.567.890.0"), "234.567.890.0 is not a decimal");

        assertNotEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("1.0.0"), "1.0.0 is not a decimal");
        assertNotEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("String"), "String is not a decimal");
        assertNotEquals(NumberUtils.NumberType.DECIMAL, NumberUtils.getNumberType("2020-20-11"), "2020-20-11 is not a decimal");

    }


    @Test
    @DisplayName("testing for DQG recommendation")
    void followsDQG(){
        assertTrue(NumberUtils.followsDQGRecommendation("0.53"));
        assertTrue(NumberUtils.followsDQGRecommendation("-1.0"));
        assertTrue(NumberUtils.followsDQGRecommendation("0.0"));
        assertTrue(NumberUtils.followsDQGRecommendation("789654"));
        assertTrue(NumberUtils.followsDQGRecommendation("25026.8"));
        assertTrue(NumberUtils.followsDQGRecommendation("789.654")); // this only looks like a decimal, but it is actually an integer with thousands separator. We cannot know, so we won't show an error for it.


        assertFalse(NumberUtils.followsDQGRecommendation("0,53"));
        assertFalse(NumberUtils.followsDQGRecommendation("789 654"));
        assertFalse(NumberUtils.followsDQGRecommendation("789.654.8"));
        assertFalse(NumberUtils.followsDQGRecommendation("25.026,8"));

    }
}