package de.piveau.metrics.linter.csv.utils;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DateUtilsTest {

    @Test
    @DisplayName("Test iso8601 date regex")
    void testIso() {
        assertTrue( DateUtils.isISODateFormat("2020-01-01"), "2020-01-01");
        assertTrue( DateUtils.isISODateFormat("2020-01-01T00:00:00"), "2020-01-01T00:00:00");
        assertTrue( DateUtils.isISODateFormat("2020-01-01T00:00:00Z"), "2020-01-01T00:00:00Z");
        assertTrue( DateUtils.isISODateFormat("2020-01-01T00:00:00+00:00"), "2020-01-01T00:00:00+00:00");
        assertTrue( DateUtils.isISODateFormat("2020-01-01T00:00:00-00:00"), "2020-01-01T00:00:00-00:00");
        assertTrue( DateUtils.isISODateFormat("2020-01-01T00:00:00.000Z"), "2020-01-01T00:00:00.000Z");
        assertTrue( DateUtils.isISODateFormat("2020-01-01T00:00:00.000+00:00"), "2020-01-01T00:00:00.000+00:00");
        assertTrue( DateUtils.isISODateFormat("2020-01-01T00:00:00.000-00:00"), "2020-01-01T00:00:00.000-00:00");
        assertTrue( DateUtils.isISODateFormat("2020-01-01T00:00:00.000"), "2020-01-01T00:00:00.000");
        assertTrue( DateUtils.isISODateFormat("2020-01-01T00:00:00.000+00"), "2020-01-01T00:00:00.000+00");
        assertTrue( DateUtils.isISODateFormat("2020-01-01T00:00:00.000-00"), "2020-01-01T00:00:00.000-00");

        assertTrue( DateUtils.isISODateFormat("2020-01-01T00:00:00.000+0000"), "2020-01-01T00:00:00.000+0000");
        assertTrue( DateUtils.isISODateFormat("2020-01-01 00:00:00.000+00"), "2020-01-01 00:00:00.000+00");
        assertTrue( DateUtils.isISODateFormat("2020-01-01 00:00:00.000+0000"), "2020-01-01 00:00:00.000+0000");
        assertTrue( DateUtils.isISODateFormat("2020-01-01 00:00:00.000+00:00"), "2020-01-01 00:00:00.000+00:00");

        assertTrue(DateUtils.isISODateFormat("2020-W21-7"), "2020-W21-7");
        assertTrue(DateUtils.isISODateFormat("2020-W21-7T00:00:00"), "2020-W21-7T00:00:00");
        assertTrue(DateUtils.isISODateFormat("2020-175"), "2020-175");


        //testng with non iso8601 date format
        assertFalse( DateUtils.isISODateFormat("20200101000000"), "20200101000000");
        assertFalse( DateUtils.isISODateFormat("2020/01/01T00:00:00.000+00:00"), "2020/01/01T00:00:00.000+00:00");
        assertFalse( DateUtils.isISODateFormat("23.12.2020"), "23.12.2020");
    }


    @Test
    @DisplayName("check if string returns date format")
    void testDateFormat(){

        assertNotNull(DateUtils.getDateFormat("20200101"), "20200101");
        assertNotNull(DateUtils.getDateFormat("23-12-2020"), "23-12-2020");
        assertNotNull(DateUtils.getDateFormat("12/23/2020"), "12/23/2020");
        assertNotNull(DateUtils.getDateFormat("23.12.2020"), "23.12.2020");
        assertNotNull(DateUtils.getDateFormat("2002-01-01"), "2002-01-01");
        assertNotNull(DateUtils.getDateFormat("2020/12/23"), "2020/12/23");
        assertNotNull(DateUtils.getDateFormat("23 Januar 2020"), "23 Januar 2020");
        assertNotNull(DateUtils.getDateFormat("23 Jan 2020") , "23 Jan 2020");
        assertNotNull(DateUtils.getDateFormat("2020-12"), "2020-12");
        assertNotNull(DateUtils.getDateFormat("2020-W01"), "2020-W01");
        assertNotNull(DateUtils.getDateFormat("2020-W1"), "2020-W1");
        assertNotNull(DateUtils.getDateFormat("2020-W01-1"), "2020-W01-1");
        assertNotNull(DateUtils.getDateFormat("2020-W1-1"), "2020-W1-1");
        assertNotNull(DateUtils.getDateFormat("2020W017"), "2020W017");
        assertNotNull(DateUtils.getDateFormat("2020-175"), "2020-175");
        assertNotNull(DateUtils.getDateFormat("2020-W01-1T00:00:00"), "2020-W01-1T00:00:00");


        assertNull(DateUtils.getDateFormat("No Date"), "No Date");
        assertNull(DateUtils.getDateFormat("20200101000000"), "20200101000000");

    }

    @Test
    @DisplayName("check if string is datetime format")
    void testDateTimeFormat() {


        assertTrue(DateUtils.isDateTime("23-12-2020"), "23-12-2020");
        assertTrue(DateUtils.isDateTime("12/23/2020"), "12/23/2020");
        assertTrue(DateUtils.isDateTime("23.12.2020"), "23.12.2020");
        assertTrue(DateUtils.isDateTime("2002-01-01"), "2002-01-01");
        assertTrue(DateUtils.isDateTime("2020/12/23"), "2020/12/23");
        assertTrue(DateUtils.isDateTime("23 Januar 2020"), "23 Januar 2020");
        assertTrue(DateUtils.isDateTime("23 Jan 2020"), "23 Jan 2020");
        assertTrue(DateUtils.isDateTime("2020-12"), "2020-12");
        assertTrue(DateUtils.isDateTime("2020-W01"), "2020-W01");
        assertTrue(DateUtils.isDateTime("2020-W1"), "2020-W1");
        assertTrue(DateUtils.isDateTime("2020-W01-1"), "2020-W01-1");
        assertTrue(DateUtils.isDateTime("2020-W1-1"), "2020-W1-1");
        assertTrue(DateUtils.isDateTime("2020W017"), "2020W017");
        assertTrue(DateUtils.isDateTime("2020-175"), "2020-175");
        assertTrue(DateUtils.isDateTime("2020-W01-1T00:00:00"), "2020-W01-1T00:00:00");


        assertFalse(DateUtils.isDateTime("20200101"), "20200101");
        assertFalse(DateUtils.isDateTime("No Date"), "No Date");
        assertFalse(DateUtils.isDateTime("20200101000000"), "20200101000000");

    }

    @Test
    @Disabled
    @DisplayName("timed check date")
    void testCheckTimeDate() {
        for (int i = 0; i < 500000; i++) {
            testDateFormat();
        }
    }

    @Test
    @Disabled
    @DisplayName("timed check date time")
    void testCheckTimeDateTime() {
        for (int i = 0; i < 500000; i++) {
            testDateTimeFormat();
        }
    }

}
