package de.piveau.metrics.linter.csv.validation;

import de.piveau.metrics.linter.csv.indicators.Indicator;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FullCsvTest {


    @Test
    @DisplayName("Testing all for all indicators using a malformed CSV")
    public void testMalformedCsv() throws IOException {

        String csvContent = """
                a,b,a,,e,e
                1,10,true, 2022-01-12,string,
                2,11,false,date,string2,astring2
                3,12,true,date,string3,astring3
                4,13,false,22.07.2000,string4,astring4
                5,14,true,22.07.2001,string5,13 456
                
                """;

        File file = new File("malformed.csv");
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        FileUtils.writeStringToFile(file, csvContent, "UTF-8");

        Result result = new Result();
        Metadata metadata = new MetadataBuilder()
                .setResult(result)
                .setFileName(file.getAbsolutePath())
                .setContentType("text/csv;charset=UTF-8")
                .build();
        metadata.checkContentType().checkFile();
        CSV csv = new CSVBuilder(result).buildAndParseCSVFile("malformed.csv", "UTF-8", metadata.getDetectedSeparator().toString());
        csv.validate();

        assertEquals(12, result.getItems().size());
        file.delete();
    }


    @Test
    @DisplayName("Testing all for all indicators using a CSV with newlines in quoted fields")
    public void testCsvWithNewlinesInQuotedFields() throws IOException {

        String csvContent = """
                "a";"b";"c"
                "d";"e";"this is
                valid"
                "a";"b";"c"
                """;

        File file = new File("malformed.csv");
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        FileUtils.writeStringToFile(file, csvContent, "UTF-8");

        Result result = new Result();
        Metadata metadata = new MetadataBuilder()
                .setResult(result)
                .setFileName(file.getAbsolutePath())
                .setContentType("text/csv;charset=UTF-8")
                .build();
        metadata.checkContentType().checkFile();
        CSV csv = new CSVBuilder(result).buildAndParseCSVFile("malformed.csv", "UTF-8", metadata.getDetectedSeparator().toString());
        csv.validate();

        assertEquals(0, result.getItems().size());
        file.delete();
    }

    @Test
    @DisplayName("Regression Test propertydescription_customtariffstructure")
    public void testRegression_001() throws IOException {


        Result result = new Result();
        Metadata metadata = new MetadataBuilder()
                .setResult(result)
                .setFileName("src/test/resources/propertydescription_customstariffstructure.csv")
                .setContentType("text/csv;charset=UTF-8")
                .build();
        metadata.checkContentType().checkFile();
        CSV csv = new CSVBuilder(result)
                .buildAndParseCSVFile("src/test/resources/propertydescription_customstariffstructure.csv", "UTF-8", metadata.getDetectedSeparator().toString());
        csv.validate();
        System.out.println(result.toJson());

        assertEquals(11, result.getItems().size());
        assertEquals(1, result.getIndicators(Indicator.IndicatorType.WARNING).size());
        assertEquals(10, result.getIndicators(Indicator.IndicatorType.ERROR).size());
        assertEquals(0, result.getIndicators(Indicator.IndicatorType.INFORMATION).size());

    }
}
