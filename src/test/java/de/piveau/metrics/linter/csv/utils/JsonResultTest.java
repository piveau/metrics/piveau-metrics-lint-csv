package de.piveau.metrics.linter.csv.utils;

import de.piveau.metrics.linter.csv.indicators.InformationIndicator;
import de.piveau.metrics.linter.csv.validation.CSV;
import de.piveau.metrics.linter.csv.validation.CSVBuilder;

import de.piveau.metrics.linter.csv.validation.Result;
import io.vertx.core.json.JsonObject;
import org.junit.jupiter.api.Test;


import java.io.IOException;
import java.util.Collections;

public class JsonResultTest {

   @Test
    void JsonTest() throws IOException {
       Result result = new Result(Collections.singleton(InformationIndicator.ASSUMED_HEADER));
       CSV csv = new CSVBuilder(result).buildAndParseCSVFile("src/test/resources/simple.csv", "UTF-8", ";");

       if (csv != null) {
           csv.validate();
           System.out.println("validated");
       } else {
           System.out.println("csv is null");
       }
       System.out.println(result.toJson());

       JsonObject json = new JsonObject(result.toJson());


       assert json.containsKey("passed") && json.getBoolean("passed", false);
       assert json.containsKey("warnings");
       assert json.containsKey("errors");
       assert json.containsKey("info");
       JsonObject info = json.getJsonObject("info");

       assert info.containsKey("count");
       assert info.getInteger("count")==1;



   }
}
