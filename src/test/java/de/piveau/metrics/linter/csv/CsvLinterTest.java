package de.piveau.metrics.linter.csv;

import de.piveau.metrics.linter.csv.indicators.ErrorIndicator;
import de.piveau.metrics.linter.csv.validation.Result;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class CsvLinterTest {

    @Test
    @DisplayName("Test csv linter class 001")
    void testCsvLinter_001() throws IOException {

        String csvContent = """
                a,b,a,,e,e
                1,10,true, 2022-01-12,string,
                2,11,false,date,string2,astring2
                3,12,true,date,string3,astring3
                4,13,false,22.07.2000,string4,astring4
                5,14,true,22.07.2001,string5,13 456
                                
                """;

        File file = new File("malformed.csv");
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        FileUtils.writeStringToFile(file, csvContent, "UTF-8");


        CSVLinter csvLinter = new CSVLinter("malformed.csv", "UTF-8");
        Result result = csvLinter.validate();

        file.delete();

        System.out.println(result.toJson());
        assertEquals(3, result.getItems().size());


    }

    @Test
    @DisplayName("Test csv linter class 001 b")
    void testCsvLinter_001_b() throws IOException {

        String csvContent = """
                a,b,a,,e,e
                1,10,true, 2022-01-12,string,
                2,11,false,date,string2,astring2
                3,12,true,date,string3,astring3
                4,13,false,22.07.2000,string4,astring4
                5,14,true,22.07.2001,string5,13 456
                                
                """;

        File file = new File("malformed.csv");
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        FileUtils.writeStringToFile(file, csvContent, "UTF-8");


        CSVLinter csvLinter = new CSVLinter("malformed.csv", "text/csv;charset=UTF-8");
        Result result = csvLinter.validate();

        file.delete();

        System.out.println(result.toJson());
        assertEquals(12, result.getItems().size());


    }

    @Test
    @DisplayName("Test csv linter class 002")
    void testCsvLinter_002() throws IOException {

        String csvContent = """
                "a";"b";"c"
                "d";"e";"this is
                valid"
                "a";"b";"c"
                """;

        File file = new File("malformed.csv");
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        FileUtils.writeStringToFile(file, csvContent, "UTF-8");


        CSVLinter csvLinter = new CSVLinter("malformed.csv", "UTF-8");
        Result result = csvLinter.validate();

        file.delete();

        System.out.println(result.toJson());
        assertEquals(2, result.getItems().size());


    }

    @Test
    @DisplayName("Test csv linter class 002 b")
    void testCsvLinter_002_b() throws IOException {

        String csvContent = """
                "a";"b";"c"
                "d";"e";"this is
                valid"
                "a";"b";"c"
                """;

        File file = new File("malformed.csv");
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        FileUtils.writeStringToFile(file, csvContent, "UTF-8");


        CSVLinter csvLinter = new CSVLinter("malformed.csv", "text/csv;charset=UTF-8");
        Result result = csvLinter.validate();

        file.delete();

        System.out.println(result.toJson());
        assertEquals(0, result.getItems().size());


    }

    @Test
    @DisplayName("Test csv linter class 002 c")
    void testCsvLinter_002_c() throws IOException {

        String csvContent = """
                "a";"b";"c"
                "d";"e";"this is
                valid"
                "a";"b";"c"
                """;

        File file = new File("malformed.csv");
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        FileUtils.writeStringToFile(file, csvContent, "UTF-8");


        CSVLinter csvLinter = new CSVLinter("malformed.csv", "text/html;charset=UTF-8");
        Result result = csvLinter.validate();

        file.delete();

        System.out.println(result.toJson());
        assertEquals(1, result.getItems().size(), "Content type should have thrown an error");
        assertTrue(result.checkingIndicator(ErrorIndicator.WRONG_CONTENT_TYPE), "Content type should have thrown an error");
        assertFalse(result.isPassed(), "should not have passed with content type html");


    }

    @Test
    @DisplayName("Test csv linter class 003")
    void testCsvLinter_003() throws IOException {


        CSVLinter csvLinter = new CSVLinter("src/test/resources/propertydescription_customstariffstructure.csv", "text/csv;charset=UTF-8");
        Result result = csvLinter.validate();

        System.out.println(result.toJson());
        assertEquals(11, result.getItems().size());


    }
}
