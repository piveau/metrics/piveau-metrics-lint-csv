package de.piveau.metrics.linter.csv.validation;


import de.piveau.metrics.linter.csv.indicators.ErrorIndicator;
import de.piveau.metrics.linter.csv.indicators.Indicator;
import de.piveau.metrics.linter.csv.indicators.InformationIndicator;
import de.piveau.metrics.linter.csv.indicators.WarningIndicator;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

public class SingleIndicatorTest {


    void testSingleIndicatorCSVBuilder(Indicator indicator, String cSVPath, String charset, String delimiter, Boolean shouldContainIndicator, Boolean checkForIndicator) throws IOException {
        Result result;
        if (checkForIndicator) {
            result = new Result(Collections.singleton(indicator));
        } else {
            result = new Result(new HashSet<>());
        }


        CSV csv = new CSVBuilder(result).buildAndParseCSVFile(cSVPath, charset, delimiter);
        if (csv != null) {
            csv.validate();
        } else {
            System.out.println("csv is null");
        }
        result.getItems().forEach(i -> System.out.println("Indicator: " + i.toString()));
        if (shouldContainIndicator) {
            assertTrue(result.contains(indicator), "Result should contain indicator " + indicator.toString());
            switch (indicator.getType()) {
                case INFORMATION ->
                        assertFalse(result.hasErrors() || result.hasWarnings(), "Result should not contain any errors or warnings");
                case WARNING ->
                        assertFalse(result.hasErrors() || result.hasInfos(), "Result should not contain any errors or infos");
                case ERROR ->
                        assertFalse(result.hasWarnings() || result.hasInfos(), "Result should not contain any warnings or infos");
            }
        } else {
            assertFalse(result.contains(indicator), "Indicator should not be contained: " + indicator.toString());
            assertEquals(0, result.getItems().size(), "Result should not contain any indicators");
        }


    }

    Metadata testingSingleIndicatorMetadataBuilder(Indicator indicator, String cSVPath, String contentType, Boolean shouldContainIndicator, Boolean checkForIndicator) throws IOException {
        Result result;
        if (checkForIndicator) {
            result = new Result(Collections.singleton(indicator));
        } else {
            result = new Result(new HashSet<>());
        }
        Metadata metadata = new MetadataBuilder().setResult(result).setFileName(cSVPath).setContentType(contentType).build();

        metadata.checkContentType();
        metadata.checkFile();
        result.getItems().forEach(i -> System.out.println("Indicator: " + i.toString()));
        if (shouldContainIndicator) {
            assertTrue(result.contains(indicator), "Result should contain indicator " + indicator.toString());
            switch (indicator.getType()) {
                case INFORMATION ->
                        assertFalse(result.hasErrors() || result.hasWarnings(), "Result should not contain any errors or warnings");
                case WARNING ->
                        assertFalse(result.hasErrors() || result.hasInfos(), "Result should not contain any errors or infos");
                case ERROR ->
                        assertFalse(result.hasWarnings() || result.hasInfos(), "Result should not contain any warnings or infos");
            }

        } else {
            assertFalse(result.contains(indicator), "Indicator should not be contained: " + indicator.toString());
            assertEquals(0, result.getItems().size(), "Result should not contain any indicators");
        }
        return metadata;
    }


    @AfterEach
    void cleanup() {
        File file = new File("testfile.csv");
        if (file.exists()) {
            file.delete();
        }
    }

    void createCSVFile(String content) throws IOException {
        File file = new File("testfile.csv");
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        FileUtils.writeStringToFile(file, content, "UTF-8");
    }


    @Test
    @DisplayName("Test Single Indicator NONRFC_LINE_BREAKS")
    void testSingleIndicatorNONRFC_LINE_BREAKS() throws IOException {

/*
        TODO: Git is changing the line breaks in the file when comitting. this makes these tests fail.


        testSingleIndicatorCSVBuilder(InformationIndicator.NONRFC_LINE_BREAKS, "src/test/resources/simple.csv", "UTF-8", ",", false, true);
        testingSingleIndicatorMetadataBuilder(InformationIndicator.NONRFC_LINE_BREAKS, "src/test/resources/simple.csv", "text/csv;charset=UTF-8", false, true);
        testingSingleIndicatorMetadataBuilder(InformationIndicator.NONRFC_LINE_BREAKS, "src/test/resources/simple.csv", null, false, true);
*/

        // CSV with standard LF line breaks
        String csv = """
                a,b,c
                d,e,f
                g,h,i
                """;
        createCSVFile(csv);

        testSingleIndicatorCSVBuilder(InformationIndicator.NONRFC_LINE_BREAKS, "testfile.csv", "UTF-8", ",", false, true);
        testingSingleIndicatorMetadataBuilder(InformationIndicator.NONRFC_LINE_BREAKS, "testfile.csv", "text/csv;charset=UTF-8", true, true);
        testingSingleIndicatorMetadataBuilder(InformationIndicator.NONRFC_LINE_BREAKS, "testfile.csv", null, true, true);

        //testing without adding to checkFor list, should not be contained in result
        testingSingleIndicatorMetadataBuilder(InformationIndicator.NONRFC_LINE_BREAKS, "testfile.csv", null, false, false);


        // CSV with CR line breaks
        csv = "a2,b,c\rd2,e,f\rg2,h,i\r";
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(InformationIndicator.NONRFC_LINE_BREAKS, "testfile.csv", "UTF-8", ",", false, true);
        testingSingleIndicatorMetadataBuilder(InformationIndicator.NONRFC_LINE_BREAKS, "testfile.csv", "text/csv;charset=UTF-8", true, true);
        testingSingleIndicatorMetadataBuilder(InformationIndicator.NONRFC_LINE_BREAKS, "testfile.csv", null, true, true);

        // CSV with CRLF line breaks
        csv = "a3,b,c\r\nd3,e,f\r\ng3,h,i";
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(InformationIndicator.NONRFC_LINE_BREAKS, "testfile.csv", "UTF-8", ",", false, true);
        testingSingleIndicatorMetadataBuilder(InformationIndicator.NONRFC_LINE_BREAKS, "testfile.csv", "text/csv;charset=UTF-8", false, true);
        testingSingleIndicatorMetadataBuilder(InformationIndicator.NONRFC_LINE_BREAKS, "testfile.csv", null, false, true);


        // CSV with CRLF line breaks and empty line
        csv = "a3,b,c\r\nd3,e,f\r\ng3,h,i\r\n";
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(InformationIndicator.NONRFC_LINE_BREAKS, "testfile.csv", "UTF-8", ",", false, true);
        testingSingleIndicatorMetadataBuilder(InformationIndicator.NONRFC_LINE_BREAKS, "testfile.csv", "text/csv;charset=UTF-8", false, true);
        testingSingleIndicatorMetadataBuilder(InformationIndicator.NONRFC_LINE_BREAKS, "testfile.csv", null, false, true);

    }



    @Test
    @DisplayName("Test Single Indicator ASSUMED_HEADER")
    void testSingleIndicatorASSUMED_HEADER() throws IOException {
        testSingleIndicatorCSVBuilder(InformationIndicator.ASSUMED_HEADER, "src/test/resources/simple.csv", "text/csv;charset=UTF-8", ",", true, true);
        testSingleIndicatorCSVBuilder(InformationIndicator.ASSUMED_HEADER, "src/test/resources/simple.csv", "UTF-8", ",", true, true);
        testSingleIndicatorCSVBuilder(InformationIndicator.ASSUMED_HEADER, "src/test/resources/simple.csv", null, ",", false, false);

        testingSingleIndicatorMetadataBuilder(InformationIndicator.ASSUMED_HEADER, "src/test/resources/simple.csv", "text/csv;charset=UTF-8", false, true);
        testingSingleIndicatorMetadataBuilder(InformationIndicator.ASSUMED_HEADER, "src/test/resources/simple.csv", "UTF-8", false, false);
    }

    @Test
    @DisplayName("Test Single Indicator NO_ENCODING")
    void testSingleIndicatorNO_ENCODING() throws IOException {
        testSingleIndicatorCSVBuilder(WarningIndicator.NO_ENCODING, "src/test/resources/simple.csv", "text/csv;charset=UTF-8", ",", false, true);
        testSingleIndicatorCSVBuilder(WarningIndicator.NO_ENCODING, "src/test/resources/simple.csv", "text/csv", ",", false, true);


        testingSingleIndicatorMetadataBuilder(WarningIndicator.NO_ENCODING, "src/test/resources/simple.csv", "text/csv;charset=UTF-8", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.NO_ENCODING, "src/test/resources/simple.csv", "text/csv;charset=GB2312", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.NO_ENCODING, "src/test/resources/simple.csv", "text/csv", true, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.NO_ENCODING, "src/test/resources/simple.csv", "UTF-8", true, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.NO_ENCODING, "src/test/resources/simple.csv", null, false, true);
    }

    @Test
    @DisplayName("Test Single Indicator ENCODING")
    void testSingleIndicatorENCODING() throws IOException {
        testSingleIndicatorCSVBuilder(WarningIndicator.ENCODING, "src/test/resources/simple.csv", "text/csv;charset=UTF-8", ",", false, true);
        testSingleIndicatorCSVBuilder(WarningIndicator.ENCODING, "src/test/resources/simple.csv", "text/csv", ",", false, true);


        testingSingleIndicatorMetadataBuilder(WarningIndicator.ENCODING, "src/test/resources/simple.csv", "text/csv;charset=UTF-8", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.ENCODING, "src/test/resources/simple.csv", "text/csv;charset=GB2312", true, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.ENCODING, "src/test/resources/simple.csv", "text/csv", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.ENCODING, "src/test/resources/simple.csv", "UTF-8", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.ENCODING, "src/test/resources/simple.csv", null, false, true);
    }

    @Test
    @DisplayName("Test Single Indicator NO_CONTENT_TYPE")
    void testSingleIndicatorNO_CONTENT_TYPE() throws IOException {
        testSingleIndicatorCSVBuilder(WarningIndicator.NO_CONTENT_TYPE, "src/test/resources/simple.csv", "text/csv;charset=UTF-8", ",", false, true);
        testSingleIndicatorCSVBuilder(WarningIndicator.NO_CONTENT_TYPE, "src/test/resources/simple.csv", "text/csv", ",", false, true);


        testingSingleIndicatorMetadataBuilder(WarningIndicator.NO_CONTENT_TYPE, "src/test/resources/simple.csv", "text/csv;charset=UTF-8", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.NO_CONTENT_TYPE, "src/test/resources/simple.csv", "text/csv;charset=GB2312", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.NO_CONTENT_TYPE, "src/test/resources/simple.csv", "text/csv", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.NO_CONTENT_TYPE, "src/test/resources/simple.csv", "UTF-8", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.NO_CONTENT_TYPE, "src/test/resources/simple.csv", null, true, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.NO_CONTENT_TYPE, "src/test/resources/simple.csv", "", true, true);
    }

    @Test
    @DisplayName("Test Single Indicator EXCEL")
    void testSingleIndicatorEXCEL() throws IOException {
        testSingleIndicatorCSVBuilder(WarningIndicator.EXCEL, "src/test/resources/excel.xls", "text/csv;charset=UTF-8", ",", false, true);
        testSingleIndicatorCSVBuilder(WarningIndicator.EXCEL, "src/test/resources/excel.xls", "text/csv", ",", false, true);


        testingSingleIndicatorMetadataBuilder(WarningIndicator.EXCEL, "src/test/resources/excel.xls", "text/csv;charset=UTF-8", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.EXCEL, "src/test/resources/excel.xlsx", "text/csv;charset=GB2312", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.EXCEL, "src/test/resources/excel.xls", "text/csv", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.EXCEL, "src/test/resources/excel.xlsx", "UTF-8", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.EXCEL, "src/test/resources/excel.xls", null, true, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.EXCEL, "src/test/resources/excel.xlsx", null, true, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.EXCEL, "src/test/resources/excel.xls", "", true, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.EXCEL, "src/test/resources/excel.xlsx", "", true, true);

        testingSingleIndicatorMetadataBuilder(WarningIndicator.EXCEL, "src/test/resources/simple.csv", null, false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.EXCEL, "src/test/resources/simple.csv", "", false, true);
    }



    @Test
    @DisplayName("Test Single Indicator CHECK_OPTIONS")
    void testSingleIndicatorCHECK_OPTIONS() throws IOException {


        //csv with semicolon seperator

        testSingleIndicatorCSVBuilder(WarningIndicator.CHECK_OPTIONS, "src/test/resources/simple.csv", "UTF-8", ";", false, true);
        testSingleIndicatorCSVBuilder(WarningIndicator.CHECK_OPTIONS, "src/test/resources/simple.csv", "UTF-8", ",", true, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.CHECK_OPTIONS, "src/test/resources/simple.csv", "text/csv;charset=UTF-8", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.CHECK_OPTIONS, "src/test/resources/simple.csv", null, false, true);


        // CSV with comma separator
        String csv = "a3,b,c\r\nd3,e,f\r\ng3,h,i";
        createCSVFile(csv);

        testSingleIndicatorCSVBuilder(WarningIndicator.CHECK_OPTIONS, "testfile.csv", "UTF-8", ",", false, true);
        testSingleIndicatorCSVBuilder(WarningIndicator.CHECK_OPTIONS, "testfile.csv", "UTF-8", ";", true, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.CHECK_OPTIONS, "testfile.csv", "text/csv;charset=UTF-8", false, true);

        //CSV with pipe separator
        csv = "a3|b|c\r\nd3|e|f\r\ng3|h|i";
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(WarningIndicator.CHECK_OPTIONS, "testfile.csv", "UTF-8", "|", false, true);
        testSingleIndicatorCSVBuilder(WarningIndicator.CHECK_OPTIONS, "testfile.csv", "UTF-8", ";", true, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.CHECK_OPTIONS, "testfile.csv", "text/csv;charset=UTF-8", false, true);
    }


    @Test
    @DisplayName("Test Single Indicator INCONSISTENT_VALUES")
    void testSingleIndicatorINCONSISTENT_VALUES() throws IOException {


        // long CSV with different data types
        String csv = """
                Text,Number,Date,Boolean,Null
                a,1,2020-01-01,true,null
                b,2,2020-01-02,false,null
                c,3,2020-01-03,true,null
                d,4,2020-01-04,false,null
                e,5,2020-01-05,true,null
                f,6,2020-01-06,false,null
                g,7,2020-01-07,true,null
                h,8,2020-01-08,false,null
                i,9,2020-01-09,true,null
                j,10,2020-01-10,false,null
                k,11,2020-01-11,true,null
                l,12,2020-01-12T23:12:01,false,null
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(WarningIndicator.INCONSISTENT_VALUES, "testfile.csv", "UTF-8", ",", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.INCONSISTENT_VALUES, "testfile.csv", "text/csv;charset=UTF-8", false, true);

        //long CSV with too many inconsistent data types in the number column
        csv = """
                Text,Number,Date,Boolean,Null
                a,a,2020-01-01,true,null
                b,b,2020-01-02,false,null
                c,3,2020-01-03,true,null
                d,4,2020-01-04,false,null
                e,5,2020-01-05,true,null
                f,6,2020-01-06,false,null
                g,7,2020-01-07,true,null
                h,8,2020-01-08,false,null
                i,9,2020-01-09,true,null
                j,10,2020-01-10,false,null
                k,11,2020-01-11,true,null
                l,12,2020-01-12T23:12:01,false,null
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(WarningIndicator.INCONSISTENT_VALUES, "testfile.csv", "UTF-8", ",", true, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.INCONSISTENT_VALUES, "testfile.csv", "text/csv;charset=UTF-8", false, true);

        //long CSV with okay amount of inconsistent data types in the number column (<90% of values)
        csv = """
                Text,Number,Date,Boolean,Null
                a,a,2020-01-01,true,null
                b,2,2020-01-02,false,null
                c,3,2020-01-03,true,null
                d,4,2020-01-04,false,null
                e,5,2020-01-05,true,null
                f,6,2020-01-06,false,null
                g,7,2020-01-07,true,null
                h,8,2020-01-08,false,null
                i,9,2020-01-09,true,null
                j,10,2020-01-10,false,null
                k,11,2020-01-11,true,null
                l,12,2020-01-12T23:12:01,false,null
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(WarningIndicator.INCONSISTENT_VALUES, "testfile.csv", "UTF-8", ",", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.INCONSISTENT_VALUES, "testfile.csv", "text/csv;charset=UTF-8", false, true);

    }

    @Test
    @DisplayName("Test Single Indicator EMPTY_COLUMN_NAME")
    void testSingleIndicatorEMPTY_COLUMN_NAME() throws IOException {

        //long CSV with empty column names
        String csv = """
                ,Number,Date,Boolean,Null
                a,1,2020-01-01,true,null
                b,2,2020-01-02,false,null
                c,3,2020-01-03,true,null
                d,4,2020-01-04,false,null
                e,5,2020-01-05,true,null
                f,6,2020-01-06,false,null
                g,7,2020-01-07,true,null
                h,8,2020-01-08,false,null
                i,9,2020-01-09,true,null
                j,10,2020-01-10,false,null
                k,11,2020-01-11,true,null
                l,12,2020-01-12T23:12:01,false,null
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(WarningIndicator.EMPTY_COLUMN_NAME, "testfile.csv", "UTF-8", ",", true, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.EMPTY_COLUMN_NAME, "testfile.csv", "text/csv;charset=UTF-8", false, true);

        //long CSV with empty column names and too many empty columns
        csv = """
                ,,Date,Boolean,Null
                a,1,2020-01-01,true,null
                b,2,2020-01-02,false,null
                c,3,2020-01-03,true,null
                d,4,2020-01-04,false,null
                e,5,2020-01-05,true,null
                f,6,2020-01-06,false,null
                g,7,2020-01-07,true,null
                h,8,2020-01-08,false,null
                i,9,2020-01-09,true,null
                j,10,2020-01-10,false,null
                k,11,2020-01-11,true,null
                l,12,2020-01-12T23:12:01,false,null
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(WarningIndicator.EMPTY_COLUMN_NAME, "testfile.csv", "UTF-8", ",", true, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.EMPTY_COLUMN_NAME, "testfile.csv", "text/csv;charset=UTF-8", false, true);

        //CSV
        csv = """
                Text,Number,Date,Boolean,Null
                a,1,2020-01-01,true,null
                b,2,2020-01-02,false,null
                c,3,2020-01-03,true,null
                d,4,2020-01-04,false,null
                e,5,2020-01-05,true,null
                f,6,2020-01-06,false,null
                g,7,2020-01-07,true,null
                h,8,2020-01-08,false,null
                i,9,2020-01-09,true,null
                j,10,2020-01-10,false,null
                k,11,2020-01-11,true,null
                l,12,2020-01-12T23:12:01,false,null
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(WarningIndicator.EMPTY_COLUMN_NAME, "testfile.csv", "UTF-8", ",", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.EMPTY_COLUMN_NAME, "testfile.csv", "text/csv;charset=UTF-8", false, true);
    }

    @Test
    @DisplayName("Test Single Indicator DUPLICATE_COLUMN_NAME")
    void testSingleIndicatorDUPLICATE_COLUMN_NAME() throws IOException {

        //CSV with duplicate column names
        String csv = """
                a,b,c,a,e
                a,1,2020-01-01,true,null
                b,2,2020-01-02,false,null
                c,3,2020-01-03,true,null
                d,4,2020-01-04,false,null
                e,5,2020-01-05,true,null
                f,6,2020-01-06,false,null
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(WarningIndicator.DUPLICATE_COLUMN_NAME, "testfile.csv", "UTF-8", ",", true, true);
        testSingleIndicatorCSVBuilder(WarningIndicator.DUPLICATE_COLUMN_NAME, "testfile.csv", "UTF-8", ",", false, false);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.DUPLICATE_COLUMN_NAME, "testfile.csv", "text/csv;charset=UTF-8", false, true);



        //CSV with 4 duplicate column names
        csv = """
                a,b,c,a,c
                a,1,2020-01-01,true,null
                b,2,2020-01-02,false,null
                c,3,2020-01-03,true,null
                d,4,2020-01-04,false,null
                e,5,2020-01-05,true,null
                f,6,2020-01-06,false,null
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(WarningIndicator.DUPLICATE_COLUMN_NAME, "testfile.csv", "UTF-8", ",", true, true);
        testSingleIndicatorCSVBuilder(WarningIndicator.DUPLICATE_COLUMN_NAME, "testfile.csv", "UTF-8", ",", false, false);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.DUPLICATE_COLUMN_NAME, "testfile.csv", "text/csv;charset=UTF-8", false, true);


        //CSV with no duplicate column names
        csv = """
                a,b,c,d,e
                a,1,2020-01-01,true,null
                b,2,2020-01-02,false,null
                c,3,2020-01-03,true,null
                d,4,2020-01-04,false,null
                e,5,2020-01-05,true,null
                f,6,2020-01-06,false,null
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(WarningIndicator.DUPLICATE_COLUMN_NAME, "testfile.csv", "UTF-8", ",", false, true);
        testSingleIndicatorCSVBuilder(WarningIndicator.DUPLICATE_COLUMN_NAME, "testfile.csv", "UTF-8", ",", false, false);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.DUPLICATE_COLUMN_NAME, "testfile.csv", "text/csv;charset=UTF-8", false, true);


    }

    @Test
    @DisplayName("Test Single Indicator TITLE_ROW")
    void testSingleIndicatorTITLE_ROW() throws IOException {

        //CSV with title row
        String csv = """
                A title
                a,b,c,a,e
                a,1,2020-01-01,true,null
                b,2,2020-01-02,false,null
                c,3,2020-01-03,true,null
                d,4,2020-01-04,false,null
                e,5,2020-01-05,true,null
                f,6,2020-01-06,false,null
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(WarningIndicator.TITLE_ROW, "testfile.csv", "UTF-8", ",", true, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.TITLE_ROW, "testfile.csv", "text/csv;charset=UTF-8", false, true);

        //CSV with no title row
        csv = """
                a,b,c,a,e
                a,1,2020-01-01,true,null
                b,2,2020-01-02,false,null
                c,3,2020-01-03,true,null
                d,4,2020-01-04,false,null
                e,5,2020-01-05,true,null
                f,6,2020-01-06,false,null
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(WarningIndicator.TITLE_ROW, "testfile.csv", "UTF-8", ",", false, true);
        testingSingleIndicatorMetadataBuilder(WarningIndicator.TITLE_ROW, "testfile.csv", "text/csv;charset=UTF-8", false, true);

    }


    //Error indicator tests
    @Test
    @DisplayName("Test Error Indicator DATE_FORMAT")
    void testSingleIndicatorDATE_FORMAT() throws IOException {
        //CSV with date format error
        String csv = """
                a,b,c
                a,1,2020-01-01
                b,2,17.12.2022
                c,3,11/20/2020
                d,4,2020-33-45
                """;


        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(ErrorIndicator.DATE_FORMAT, "testfile.csv", "UTF-8", ",", true, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.DATE_FORMAT, "testfile.csv", "text/csv;charset=UTF-8", false, true);

        //CSV with no date format error, either date is correct or it is not recognised as date
        csv = """
                a,b,c
                a,1,2020-01-01
                b,2,no date
                c,3,20200101
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(ErrorIndicator.DATE_FORMAT, "testfile.csv", "UTF-8", ",", false, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.DATE_FORMAT, "testfile.csv", "text/csv;charset=UTF-8", false, true);

    }

    @Test
    @DisplayName("Test Error Indicator DELIMITER")
    void testSingleIndicatorDELIMITER() throws IOException {
        //CSV comma as delimiter
        String csv = """
                a,b,c
                a,1,2020-01-01
                b,2,17.12.2022
                c,3,11/20/2020
                """;
        createCSVFile(csv);
        Metadata m = testingSingleIndicatorMetadataBuilder(WarningIndicator.DELIMITER, "testfile.csv", "text/csv;charset=UTF-8", false, true);
        if(m.getDetectedSeparator() != null) {

            assertEquals(",", m.getDetectedSeparator().toString());
        }
        assertTrue(m.getResult().contains(WarningIndicator.DELIMITER));
        testSingleIndicatorCSVBuilder(WarningIndicator.DELIMITER, "testfile.csv", "UTF-8", m.getDetectedSeparator().toString(), true, true);




        //CSV semicolon as delimiter
        csv = """
                a;b;c
                a;1;2020-01-01
                b;2;17.12.2022
                c;3;11/20/2020
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(WarningIndicator.DELIMITER, "testfile.csv", "UTF-8", ";", false, true);
        m = testingSingleIndicatorMetadataBuilder(WarningIndicator.DELIMITER, "testfile.csv", "text/csv;charset=UTF-8", false, true);
        if(m.getDetectedSeparator() != null) {

            assertEquals(";", m.getDetectedSeparator().toString());
        }
        assertFalse(m.getResult().contains(WarningIndicator.DELIMITER));
        testSingleIndicatorCSVBuilder(WarningIndicator.DELIMITER, "testfile.csv", "UTF-8", m.getDetectedSeparator().toString(), false, true);



        //CSV tab as delimiter
        csv = """
                a\tb\tc
                a\t1\t2020-01-01
                b\t2\t17.12.2022
                c\t3\t11/20/2020
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(WarningIndicator.DELIMITER, "testfile.csv", "UTF-8", "\t", true, true);
        m = testingSingleIndicatorMetadataBuilder(WarningIndicator.DELIMITER, "testfile.csv", "text/csv;charset=UTF-8", false, true);
        if(m.getDetectedSeparator() != null) {

            assertEquals("\t", m.getDetectedSeparator().toString());
        }
        assertTrue(m.getResult().contains(WarningIndicator.DELIMITER));

        testSingleIndicatorCSVBuilder(WarningIndicator.DELIMITER, "testfile.csv", "UTF-8", m.getDetectedSeparator().toString(), true, true);


        //CSV with semicolon as delimiter, but commas inside quoted fields
        csv = """
                "a,,";"b,";",c,"
                "d,";",e";"this, is,,,
                ,valid"
                ",a,,";"b,";",,,c,"
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(WarningIndicator.DELIMITER, "testfile.csv", "UTF-8", ";", false, true);
        m = testingSingleIndicatorMetadataBuilder(WarningIndicator.DELIMITER, "testfile.csv", "text/csv;charset=UTF-8", false, true);
        if(m.getDetectedSeparator() != null) {

            assertEquals(";", m.getDetectedSeparator().toString());
        }
        assertFalse(m.getResult().contains(WarningIndicator.DELIMITER));

        testSingleIndicatorCSVBuilder(WarningIndicator.DELIMITER, "testfile.csv", "UTF-8", m.getDetectedSeparator().toString(), false, true);





    }


    @Test
    @DisplayName("Test Error Indicator NULL_VALUES")
    void testSingleIndicatorNULL_VALUES() throws IOException {
        //CSV with incorrectly marked null values
        String csv = """
                a,b,c
                ,1,2020-01-01
                b,2,""
                c,3,11/20/2020
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(ErrorIndicator.NULL_VALUES, "testfile.csv", "UTF-8", ",", true, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.NULL_VALUES, "testfile.csv", "text/csv;charset=UTF-8", false, true);

        //CSV with correct null values
        csv = """
                a,b,c
                a,1,NULL
                b,null,17.12.2022
                c,3,null
                na,4, 20000
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(ErrorIndicator.NULL_VALUES, "testfile.csv", "UTF-8", ",", false, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.NULL_VALUES, "testfile.csv", "text/csv;charset=UTF-8", false, true);
    }


    @Test
    @DisplayName("Test Error Indicator WRONG_CONTENT_TYPE")
    void testSingleIndicatorWRONG_CONTENT_TYPE() throws IOException {
        testSingleIndicatorCSVBuilder(ErrorIndicator.WRONG_CONTENT_TYPE, "src/test/resources/simple.csv", "text/csv;charset=UTF-8", ",", false, true);
        testSingleIndicatorCSVBuilder(ErrorIndicator.WRONG_CONTENT_TYPE, "src/test/resources/simple.csv", "text/csv", ",", false, true);


        testingSingleIndicatorMetadataBuilder(ErrorIndicator.WRONG_CONTENT_TYPE, "src/test/resources/simple.csv", "text/csv;charset=UTF-8", false, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.WRONG_CONTENT_TYPE, "src/test/resources/simple.csv", "text/csv;charset=GB2312", false, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.WRONG_CONTENT_TYPE, "src/test/resources/simple.csv", "text/csv", false, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.WRONG_CONTENT_TYPE, "src/test/resources/simple.csv", "application/pdf;charset=UTF-8", true, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.WRONG_CONTENT_TYPE, "src/test/resources/simple.csv", "UTF-8", true, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.WRONG_CONTENT_TYPE, "src/test/resources/simple.csv", null, false, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.WRONG_CONTENT_TYPE, "src/test/resources/simple.csv", "", false, true);
    }

    @Test
    @DisplayName("Test Error Indicator RAGGED_ROWS")
    void testSingleIndicatorRAGGED_ROWS() throws IOException {
        //CSV with rows with different number of columns
        String csv = """
                a,b,c
                a,1,2020-01-01
                b,2
                c,3,11/20/2020,true
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(ErrorIndicator.RAGGED_ROWS, "testfile.csv", "UTF-8", ",", true, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.RAGGED_ROWS, "testfile.csv", "text/csv;charset=UTF-8", false, true);

        //CSV where all rows have the same number of columns
        csv = """
                a,b,c
                a,1,2020-01-01
                b,2,""
                c,3,11/20/2020
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(ErrorIndicator.RAGGED_ROWS, "testfile.csv", "UTF-8", ",", false, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.RAGGED_ROWS, "testfile.csv", "text/csv;charset=UTF-8", false, true);
    }

    @Test
    @DisplayName("Test Error Indicator BLANK_ROWS")
    void testSingleIndicatorBLANK_ROWS() throws IOException {
        //CSV with blank rows
        String csv = """
                a,b,c
                a,1,2020-01-01
                
                c,3,11/20/2020
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(ErrorIndicator.BLANK_ROWS, "testfile.csv", "UTF-8", ",", true, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.BLANK_ROWS, "testfile.csv", "text/csv;charset=UTF-8", false, true);

        //csv with one row with only empty cells

        csv = """
                a,b,c
                ,,
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(ErrorIndicator.BLANK_ROWS, "testfile.csv", "UTF-8", ",", true, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.BLANK_ROWS, "testfile.csv", "text/csv;charset=UTF-8", false, true);

        //csv with one row with only empty cells

        csv = """
                a,b,c
                1,2,3
                "","",""
                4,5,6
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(ErrorIndicator.BLANK_ROWS, "testfile.csv", "UTF-8", ",", true, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.BLANK_ROWS, "testfile.csv", "text/csv;charset=UTF-8", false, true);


        //CSV with no blank rows
        csv = """
                a,b,c
                a,1,2020-01-01
                b,2,""
                c,3,11/20/2020
                """;
        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(ErrorIndicator.BLANK_ROWS, "testfile.csv", "UTF-8", ",", false, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.BLANK_ROWS, "testfile.csv", "text/csv;charset=UTF-8", false, true);
    }

    @Test
    @DisplayName("Test Error Indicator INVALID_ENCODING")
    void testSingleIndicatorINVALID_ENCODING() throws IOException {
        testSingleIndicatorCSVBuilder(ErrorIndicator.INVALID_ENCODING, "src/test/resources/test_charset_ISO-8859-1.csv", "UTF-8", ",", false, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.INVALID_ENCODING, "src/test/resources/test_charset_ISO-8859-1.csv", "text/csv;charset=UTF-8", false, true);

    /*    //CSV with invalid encoding
        String csv = """
                姓名;年龄;出生日期
                王;3 月 23 日;Número Histórico
                李;55岁;1999年12月24日
                """;
        createCSVFile(csv, "GB2312");
        testSingleIndicatorCSVBuilder(ErrorIndicator.INVALID_ENCODING, "testfile.csv", "UTF-8", ";", false, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.INVALID_ENCODING, "testfile.csv", "text/csv;charset=UTF-8", true, true);

*/
        testSingleIndicatorCSVBuilder(ErrorIndicator.INVALID_ENCODING, "src/test/resources/test_charset_GB2312.csv", "UTF-8", ";", false, true);
        //testingSingleIndicatorMetadataBuilder(ErrorIndicator.INVALID_ENCODING, "src/test/resources/test_charset_GB2312.csv", "text/csv;charset=UTF-8", true, true);
    }


    @Test
    @DisplayName("Test Error Indicator STRAY_QUOTE")
    void testSingleIndicatorSTRAY_QUOTE() throws IOException {
        //CSV with stray quotes
        String csv = """
                "a","b","c"
                "a",1,"2020-01-01"
                "b","2","2020"-01-01"
                "c","3","11/20/2020","true"
                """;
        createCSVFile(csv);

        testSingleIndicatorCSVBuilder(ErrorIndicator.STRAY_QUOTE, "testfile.csv", "UTF-8", ",", true, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.STRAY_QUOTE, "testfile.csv", "text/csv;charset=UTF-8", false, true);

        //setting ; as delimiter but delimiter is ','. All fields are surrounded by quotes, which might trip the parser, because it expects to find a ; between the quoted fields.
        csv = """
                "a","b","c"
                "a","1","2020-01-01"
                "b","2","17.12.2022"
                "c","3","11/20/2020"
                """;

        createCSVFile(csv);
        testSingleIndicatorCSVBuilder(ErrorIndicator.STRAY_QUOTE, "testfile.csv", "UTF-8", ";", true, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.STRAY_QUOTE, "testfile.csv", "text/csv;charset=UTF-8", false, true);

    }

    @Test
    @DisplayName("Test Error Indicator WHITESPACE")
    void testSingleIndicatorWHITESPACE() throws IOException {
        //CSV with stray quotes
        String csv = """
                "a","b","c"
                "a",1,"2020-01-01"
                "b",   "2","2020-01-01"
                "c","3","11/20/2020","true"
                """;
        createCSVFile(csv);

        testSingleIndicatorCSVBuilder(ErrorIndicator.WHITESPACE, "testfile.csv", "UTF-8", ",", true, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.WHITESPACE, "testfile.csv", "text/csv;charset=UTF-8", false, true);
    }

    @Test
    @DisplayName("Test Error Indicator LINE_BREAKS")
    void testSingleIndicatorLINE_BREAKS() throws IOException {
        //CSV with stray quotes
        String  csv = "a3,b,c\r\nd3,  e,f\rg3,h,i\n";
        createCSVFile(csv);

        testSingleIndicatorCSVBuilder(ErrorIndicator.LINE_BREAKS, "testfile.csv", "UTF-8", ",", false, true);
        testingSingleIndicatorMetadataBuilder(ErrorIndicator.LINE_BREAKS, "testfile.csv", "text/csv;charset=UTF-8", true, true);
    }

}
