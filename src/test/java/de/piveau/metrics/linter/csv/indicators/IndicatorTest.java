package de.piveau.metrics.linter.csv.indicators;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IndicatorTest {

    @Test
    @DisplayName("Error indicators")
    void testErrorIndicators() {
        for (Indicator indicator : ErrorIndicator.values()) {
            assertEquals(Indicator.IndicatorType.ERROR, indicator.getType());
        }

        assertEquals("unknown_error", ErrorIndicator.UNKNOWN_ERROR.getName());
        assertEquals("Unknown error", ErrorIndicator.UNKNOWN_ERROR.getMessage());
        assertEquals("date_format", ErrorIndicator.DATE_FORMAT.getName());
        assertEquals("Date formats do not conform to a standard", ErrorIndicator.DATE_FORMAT.getMessage());
        assertEquals("delimiter", WarningIndicator.DELIMITER.getName());
        assertEquals("A semicolon should be used as delimiter", WarningIndicator.DELIMITER.getMessage());
        assertEquals("null_values", ErrorIndicator.NULL_VALUES.getName());
        assertEquals("Mark null values explicitly as such", ErrorIndicator.NULL_VALUES.getMessage());
        assertEquals("wrong_content_type", ErrorIndicator.WRONG_CONTENT_TYPE.getName());
        assertEquals("Content type is not text/csv", ErrorIndicator.WRONG_CONTENT_TYPE.getMessage());
        assertEquals("ragged_rows", ErrorIndicator.RAGGED_ROWS.getName());
        assertEquals("Row has a different number of columns (than the first row in the file)", ErrorIndicator.RAGGED_ROWS.getMessage());
        assertEquals("blank_rows", ErrorIndicator.BLANK_ROWS.getName());
        assertEquals("Completely empty row, e.g. blank line or a line where all column values are empty", ErrorIndicator.BLANK_ROWS.getMessage());
        assertEquals("invalid_encoding", ErrorIndicator.INVALID_ENCODING.getName());
        assertEquals("Encoding error when parsing row, e.g. because of invalid characters", ErrorIndicator.INVALID_ENCODING.getMessage());
        assertEquals("not_found", ErrorIndicator.NOT_FOUND.getName());
        assertEquals("HTTP 404 error when retrieving the data", ErrorIndicator.NOT_FOUND.getMessage());
        assertEquals("stray_quote", ErrorIndicator.STRAY_QUOTE.getName());
        assertEquals("Missing or stray quote", ErrorIndicator.STRAY_QUOTE.getMessage());
        assertEquals("whitespace", ErrorIndicator.WHITESPACE.getName());
        assertEquals("A quoted column has leading or trailing whitespace", ErrorIndicator.WHITESPACE.getMessage());
        assertEquals("line_breaks", ErrorIndicator.LINE_BREAKS.getName());
        assertEquals("Line breaks were inconsistent or incorrectly specified", ErrorIndicator.LINE_BREAKS.getMessage());


    }

    @Test
    @DisplayName("Information indicators")
    void testInformationIndicators() {
        for (Indicator indicator : InformationIndicator.values()) {
            assertEquals(Indicator.IndicatorType.INFORMATION, indicator.getType());
        }

        assertEquals("nonrfc_line_breaks", InformationIndicator.NONRFC_LINE_BREAKS.getName());
        assertEquals("Uses non-CRLF line breaks, so doesn't conform to RFC4180", InformationIndicator.NONRFC_LINE_BREAKS.getMessage());
        assertEquals("assumed_header", InformationIndicator.ASSUMED_HEADER.getName());
        assertEquals("The validator has assumed that a header is present", InformationIndicator.ASSUMED_HEADER.getMessage());


    }

    @Test
    @DisplayName("Warning indicators")
    void testWarningIndicators() {
        for (Indicator indicator : WarningIndicator.values()) {
            assertEquals(Indicator.IndicatorType.WARNING, indicator.getType());
        }

        assertEquals("no_encoding", WarningIndicator.NO_ENCODING.getName());
        assertEquals("The Content-Type header returned in the HTTP request does not have a charset parameter", WarningIndicator.NO_ENCODING.getMessage());
        assertEquals("encoding", WarningIndicator.ENCODING.getName());
        assertEquals("The character set is not UTF-8", WarningIndicator.ENCODING.getMessage());
        assertEquals("no_content_type", WarningIndicator.NO_CONTENT_TYPE.getName());
        assertEquals("File is being served without a Content-Type header", WarningIndicator.NO_CONTENT_TYPE.getMessage());
        assertEquals("excel", WarningIndicator.EXCEL.getName());
        assertEquals("No Content-Type header and the file extension is .xls", WarningIndicator.EXCEL.getMessage());
        assertEquals("check_options", WarningIndicator.CHECK_OPTIONS.getName());
        assertEquals("CSV file appears to contain only a single column", WarningIndicator.CHECK_OPTIONS.getMessage());
        assertEquals("inconsistent_values", WarningIndicator.INCONSISTENT_VALUES.getName());
        assertEquals("Inconsistent values in the same column. Reported if <90% of values seem to have same data type (either numeric or alphanumeric including punctuation)", WarningIndicator.INCONSISTENT_VALUES.getMessage());
        assertEquals("empty_column_name", WarningIndicator.EMPTY_COLUMN_NAME.getName());
        assertEquals("A column in the CSV header has an empty name", WarningIndicator.EMPTY_COLUMN_NAME.getMessage());
        assertEquals("duplicate_column_name", WarningIndicator.DUPLICATE_COLUMN_NAME.getName());
        assertEquals("A column in the CSV header has a duplicate name", WarningIndicator.DUPLICATE_COLUMN_NAME.getMessage());
        assertEquals("title_row", WarningIndicator.TITLE_ROW.getName());
        assertEquals("There appears to be a title field in the first row of the CSV", WarningIndicator.TITLE_ROW.getMessage());



    }
}
